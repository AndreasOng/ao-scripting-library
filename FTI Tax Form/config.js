(function() {
  

  /*
  * Main Functions
  */
  wpw.main.config(['taxConfigProvider', function(conf) {
    conf.setMenuVisibility(1);
    conf.setCssRoot('prod/com.caseware.au.e.casewarefti.2019/');
    conf.setCssFiles(['exporttax.css']);
  }]);
})();