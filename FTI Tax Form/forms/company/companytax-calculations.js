/*jshint esversion: 6 */
(function() {


  wpw.tax.actions.populateAccounts = function(formId) {

    var cachedAccounts;
    var cachedTags;
    var q = wpw.tax.global.q.defer();

    /*
    function findTagByNumber(number) {
      var length = cachedTags.length;
      for (var i = 0; i < length; i++) {
        var tag = cachedTags[i];
        if (tag.number === number) {
          return tag;
        }
      }
      return undefined;
    }

    function findAccountNumbersByTag(tag) {
      var result = [];
      var length = cachedAccounts.length;
      for (var i = 0; i < length; i++) {
        var account = cachedAccounts[i];
        if (account.tags.indexOf(tag.id) !== -1 && result.indexOf(account.number) === -1) {
          result.push(account.number);
        }
      }
      return result;
    }
    */

    function populateCell(formId, cell, tagNumbers) {
      //Protection, don't change overrides
      // if the accounts have already been populated, don't set from the default mapping
      /*
      if (wpw.tax.form(formId).field(cell.num).get() !== null && typeof(wpw.tax.form(formId).field(cell.num).get()) !==
          'undefined' &&
          wpw.tax.form(formId).field(cell.num).get().trim().length > 0)//!== '')
        return;

      //Don't need this one for now
      /*
      var accountNumbers = [];
      var tags = typeof tagNumbers == 'string' ? [tagNumbers] : tagNumbers;
      tags.forEach(function(tagNumber) {
        var tag = findTagByNumber(tagNumber);
        if (tag) {
          accountNumbers = accountNumbers.concat(findAccountNumbersByTag(tag));
        }
      });
      var value = accountNumbers.sort().join(',');*/

      //build the formula
      wpw.tax.form(formId).field(cell.num).set(tagNumbers.sort().join(','));
    }

    function traverseTables(formId) {
      var tables = wpw.tax.create.retrieve('tables', formId);
      for (var table in tables) {
        if (tables.hasOwnProperty(table)) {
          var tableObj = tables[table];
          for (var irow = 0; irow < tableObj.cells.length; irow++) {
            var rowObj = tableObj.cells[irow];

            //Go through every column in the table
            for (var column in rowObj) {
              if (rowObj.hasOwnProperty(column)) {

                //Read the column Object
                var columnObj = rowObj[column];

                //check whether a num and taxInfo property has been identified
                if (angular.isDefined(columnObj.num) && angular.isDefined(columnObj.taxInfo)) {

                  //check whether we have mapping
                  if (columnObj.taxInfo['tags']) {
                    console.log(columnObj.taxInfo['tags']);
                    populateCell(formId, columnObj, columnObj.taxInfo['tags']);
                  }
                }
              }
            }
          }
        }
      }
    }

    // To get all the accounts pass null instead of first parameter
    wpw.tax.getAccounts(null, function(accounts) {
      cachedAccounts = accounts;
      // To get all the tags pass null instead of first parameter
      wpw.tax.getTags(null, function(tags) {
        cachedTags = tags;
        console.log("HERE3");
        traverseTables(formId);
        q.resolve(formId);
      }, wpw.tax.TAG_CATEGORIES['FINANCIAL'], ['id']);
    }, ['number', 'tags']);

    return q.promise;
  };

  
  
  wpw.tax.actions.getTagTotal = function (commaSeparatedTags){
    var q = wpw.tax.global.q.defer();
    
    wpw.tax.getTags(commaSeparatedTags, function(tags){
        
      console.log("XXX " + tags.length)
      total = 0.0;
      for(i=0; i<tags.length; i++){
        console.log("XXX " + parseFloat(wpw.balanceSet().getFinal(tags[i].balances)));
        total += parseFloat(wpw.balanceSet().getFinal(tags[i].balances));
      } 
      q.resolve(total);

    }, wpw.tax.TAG_CATEGORIES.FINANCIAL);

    

    return q.promise;
  };
  
  wpw.tax.actions.populateTaxBalances = function(formId) {

    var colTagsId = 4;
    var colBalId = 3;
    var tables = wpw.tax.create.retrieve('tables', formId);
    var promises = [];
    var q = wpw.tax.global.q.defer();

    for (var table in tables) {

      if (tables.hasOwnProperty(table)) {

        var tableObj = tables[table];
        for (var irow = 0; irow < tableObj.cells.length; irow++) {
          var rowObj = tableObj.cells[irow];
          console.log("HERE 4");

          var taxInfo = rowObj[colTagsId]['taxInfo'];
          
          if(taxInfo && taxInfo.tags && taxInfo.tags.length){
            promises.push(wpw.tax.actions.getTagTotal(taxInfo.tags).then(function(cellNo, sign, total) {
              wpw.tax.form(formId).field(cellNo).set(sign ? -total : total);
              wpw.tax.form(formId).field(cellNo).disabled(true);
            }.bind(null, rowObj[colBalId]['num'], rowObj[colBalId]['sign'])));
          }

/*
          var accounts = wpw.tax.form(formId).field(rowObj[colTagsId]['num']).get();
          if (accounts && accounts.length) {
            if (!wpw.tax.form(formId).field(rowObj[colBalId]['num']).overridden()) {
              promises.push(wpw.tax.actions.getAccountsTotal(accounts, false).then(function(cellNo, sign, total) {
                wpw.tax.form(formId).field(cellNo).set(sign ? -total : total);
                wpw.tax.form(formId).field(cellNo).disabled(true);
              }.bind(null, rowObj[colBalId]['num'], rowObj[colBalId]['sign'])));
            }
          }
          else {
            //wpw.tax.field(rowObj[colBalId]['num']).set(0);
            wpw.tax.form(formId).field(rowObj[colBalId]['num']).disabled(false);
          }
*/

        }
      }
    }

    wpw.tax.global.q.all(promises).then(function() {
      q.resolve(formId);
    }).catch(function(error) {
      q.reject(error);
    });

    return q.promise;
  };
  
  
  var formId = 'companytax';

  //Perform the calculation blocks
  wpw.tax.create.calcBlocks(formId, function(calcUtils) {

    calcUtils.onFormLoad(formId, function(calcUtils) {
      console.log("HERE1");
      wpw.tax.actions.populateAccounts(formId).then(function(formId) {
        wpw.tax.actions.populateTaxBalances(formId);
      });
    });


    /**
     * This block will populate the table with data and perfoms initial calculation
     */
    calcUtils.calc(function(calcUtils, field, form) {
      console.log("HERE2");  
      wpw.tax.actions.populateTaxBalances(formId);
      /*
        field('ifx_number').assign(6856);
        field('ifx_mapping').assign('060.010.030');

        field('ggn_number').assign(8555);
        field('ggn_mapping').assign('060.010.020');

        field('bmd_number').assign(5062);
        field('bmd_mapping').assign('060.010.010, 060.050, 060.070.050');

        field('bmf_number').assign(1169);
        field('bmf_mapping').assign('060.020');

        field('afz_number').assign(2247);
        field('afz_mapping').assign('060.030, 060.070.010');

        field('aid_number').assign(3864);
        field('aid_mapping').assign('060.040');

        field('dfl_number').assign(5272);
        field('dfl_mapping').assign('060.070.060');

        field('blg_number').assign(4396);
        field('blg_mapping').assign('060.070.040');

        field('iml_number').assign(5100);
        field('iml_mapping').assign('060.080');

        field('aio_number').assign(5074);
        field('aio_mapping').assign('060.070.020, 060.060');

        //field('totalincome').assign(47595);
        */

    });


  });

})();
