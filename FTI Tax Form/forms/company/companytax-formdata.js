(function() {
  'use strict';

  wpw.tax.create.formData('companytax', {
    formInfo: {
      abbreviation: 'CompanyTax',
      title: 'Company Tax Form',
      hideHeader: false,
      printTitle: 'Company Tax Form',
      procedureControls : false,
      highlightFieldsets: true,
      hideHeaderBox: false
    },//formInfo closing

    sections: [
      {
        header: 'Guidance',
        rows:[
          {
            label: 'provide guidance here'
          }
        ]
      },
      {
        header: 'Income',
        rows:[
          {
            type: 'table',
            num: 'income'
          }
        ]
      }
    ]
  });

})();