/*jshint esversion: 6 */
(function() {
  'use strict';
   
  //Prepare the tax export object
  if (!wpw.tax.global.taxExport)
    wpw.tax.global.taxExport = {};

  //Define the cell data for the Income Table
  wpw.tax.global.taxExport.cellDataIncome = {
    //Array Format
    // Tax Code, Associated Mapping Number, Description, Guidance
    rows : [
      ['IFX', ['030.010', '030.030'], 'Gross payments subject to foreign resident withholding (excluding capital gains)',`GUIDANCE TEXT`],
      ['BLG.CHR', ['010.010'], 'Assessable government industry payments',`GUIDANCE TEXT`]
    ]

  };

  //This will define the headings on the table
  var aColumnData = [
    /*0*/ {width: '5%',   type:'none'},
    /*1*/ {width: '35%',  type:'none',  header: 'Description'},
    /*2*/ {width: '10%',  type:'none',  header: 'Code'},
    /*3*/ {width: '20%',  type:'none',  header:'Amount', total: true,   totalNum: 'totalIncome',  init: 0},
    /*4*/ {width: '30%',  type:'none',  header:'Mapping'}
    ];

  /*
    * This function will convert the data that we have in the tableData array into the array that we can use for the content
    */
  wpw.tax.actions.addTableData = function(formId, tableNum, tableData) {

    //store the table details
    tableData['name'] = formId;
    tableData['num'] = tableNum;
    
    //get the row information
    var rows = tableData['rows'];
    
    //Prepare the CELLS array to return
    var cellArr = [];

    //Process the rows
    for(var i=0; i<rows.length; i++){

      //build the cell data object
      var cellData = {};
      var taxNum  = rows[i][0];

      //Building the column 

      //Column 0, the guidance
      cellData['0'] = {label: ' ', tipGuidance: rows[i][3]};

      //Column 1, the description
      cellData[1] = {label: rows[i][2]};

      //Column 3, the tax code
      cellData[2] = {
        tn: taxNum
      }

      //Column 4, the amount
      cellData[3] = {
        num: wpw.tax.actions.tn2num(taxNum + '_amount'),
        type: 'number',
        disabled: true,
        cannotOverride: true
      }

      //Column 4, the mapping
      cellData[4] = {
        num: wpw.tax.actions.tn2num(taxNum + '_mapping'),
        type: 'selector',
        textAlign: 'center',
        taxInfo: {taxCode: taxNum.toLowerCase(), tags: rows[i][1]},
        //disabled: true,
        cannotOverride: true,
        selectorOptions: {
          enableEdit: false,
          multiple: true,
          title: 'Accounts',
          items: {
            acc001:'account1 \t 10000 \t 2000',
            acc002:'account2'
          }
        }
      }



      //Push the changes to the array
      cellArr.push(cellData);
    }

    //Return the information back to the calling function
    return cellArr;
  }

  /*
  * This function converts the full stop . in the tax code into dash because the tax form
  * doesn't accept full stop
  */
  wpw.tax.actions.tn2num = function(tn) {
    return tn.replace('.', '-').toLowerCase();
  };

    wpw.tax.create.tables('companytax', {

      income: {
        type: 'table', 
        fixedRows: true,
        infoTable: true,
        num: 'income',
        hasTotals : true,
        columns: aColumnData,
        cells: wpw.tax.actions.addTableData('companytax', 'income', wpw.tax.global.taxExport.cellDataIncome)
      }
    
  });
})();
