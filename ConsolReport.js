var cw = document.cwClient;

//prepare the objects
var iGroupIndex = 1;
var aRowData = [];
var aRowSubtotal = [];
var progressBar;
var entityList = [];
var sFinalColumn;
var iFinalColumnIndex;

progressBar = document.createProgressBar("Exporting L/S", 1, 1, 0, "Exporting L/S");


progressBar.setMessage("Creating Excel Document");

var excelApp = new ActiveXObject("Excel.Application");
excelApp.Workbooks.Add();
excelApp.ActiveWorkbook.Worksheets(1).Select();
excelApp.ActiveWorkbook.Worksheets(1).Activate();

var oRowContent;
var iRowIndex = 1;


progressBar.setMessage("Reading Consolidation Structure");
//Build the entity list for the column index
var oRootEntity = cw.RootEntity;
entityList.push(oRootEntity);
processEntityStructure(oRootEntity.Children);

function processEntityStructure(oChildren){
	//Enumerate the children
	for(var ent = new Enumerator(oChildren); !ent.atEnd(); ent.moveNext()){
		
		var oEntity = ent.item();
		entityList.push(oEntity);
		
		//process additional entity below
		if(oEntity.HasChildren){
			processEntityStructure(oEntity.Children);
		}
	}
}


//Scan through the L/S Groupings
progressBar.setMessage("Reading Grouping Details");
progressBar.setTitle("Reading Grouping Details");
var oGroupings = cw.Groupings(iGroupIndex);
progressBar.setProgress(oGroupings.Count, 1);
progressBar.setPos(1);

for(var grp = new Enumerator(oGroupings); !grp.atEnd(); grp.moveNext()){
	
	progressBar.updateProgress(1);
	var oGroup = grp.item();
	
	//does the group has a normal behaviour type?
	if(oGroup && oGroup.BehaviourType == 0 && oGroup.Accounts.Count > 0){
		progressBar.setMessage(oGroup.Name + " (" + oGroup.Accounts.Count + " accounts)");
		var oGroupDetails = {
			groupId : oGroup.Id,
			groupDetail : oGroup,
			accounts: processAccounts(oGroup.Accounts)
		}
		
		aRowData.push(oGroupDetails);
	}
}



//Now let's print the first header
progressBar.setMessage("Set Header");
oRowContent = [];
for(var i=0; i<entityList.length; i++){
	oRowContent.push(entityList[i].Name);
}
oRowContent.push("Total");
excelApp.ActiveSheet.Cells(1,1).value = "Acc No.";
excelApp.ActiveSheet.Cells(1,2).value = "Acc Description";
excelApp.ActiveSheet.Cells(1,3).value = formatRowData(oRowContent);
iRowIndex++;


//Set the column A to be text only
excelApp.ActiveSheet.Range("A:A").NumberFormat = "@";


//Calculate the index of the final column to use later
iFinalColumnIndex = oRowContent.length+2;	//Need to offset by 2 for column A and B
sFinalColumn = CWANZ_numToColumn(iFinalColumnIndex);




//Now let's push through the data content
progressBar.setMessage("Writing data to Excel");
progressBar.setTitle("Writing data to Excel");
progressBar.setProgress(aRowData.length, 1);
progressBar.setPos(1);

for(var i=0; i<aRowData.length; i++){
	
	//this is the starting point
	var iSubtotalStart = iRowIndex;
	
	progressBar.updateProgress(1);
	
	
	for(var key in aRowData[i].accounts){
		var oAccount = aRowData[i].accounts[key];
		
		oRowContent = [];
		progressBar.setMessage("Writing data to Excel (" + i + "/" + aRowData.length + ") - " + key);
		for(var j=0; j<entityList.length; j++){
			//Check if we have balance
			if(oAccount[entityList[j].Abbreviation]){
				oRowContent.push(oAccount[entityList[j].Abbreviation].current);
			}
			else{
				oRowContent.push(0);
			}
		}
		
		excelApp.ActiveSheet.Cells(iRowIndex,1).value = key;
		excelApp.ActiveSheet.Cells(iRowIndex,2).value = oAccount.name;
		excelApp.ActiveSheet.Cells(iRowIndex,3).value = formatRowData(oRowContent);
		iRowIndex++;
	}
	
	aRowSubtotal.push({
		start: iSubtotalStart,
		end: iRowIndex,
		id: aRowData[i].groupId,
		name: aRowData[i].groupDetail.Name
	})
	
	excelApp.ActiveSheet.Cells(iRowIndex,1).value = "_subtotal|" + aRowData[i].groupId + "|" + aRowData[i].groupDetail.Name;
	iRowIndex++;
	iRowIndex++;
}

//Now we need to break the table row data into multiple lines
progressBar.setTitle("Apply formatting");
progressBar.setMessage("Separating data");
excelApp.ActiveSheet.Range("C:C").TextToColumns(excelApp.ActiveSheet.Range("C1"),1 /*Delimited*/, 1 /*double quote text qualifier*/, 0, 1/*Tab delimiter*/);

//Now we need to add subtotal on the last column
progressBar.setMessage("Adding subtotal on column " + sFinalColumn);
excelApp.ActiveSheet.Range(sFinalColumn + 2).formula = "=SUM(C2:" + CWANZ_numToColumn(iFinalColumnIndex-1) +"2)";
excelApp.ActiveSheet.Range(sFinalColumn + 2).Copy;
excelApp.ActiveSheet.Range(sFinalColumn + 2 + ":" + sFinalColumn + (iRowIndex-1)).Select;
excelApp.ActiveSheet.Paste;

//lets bold it too
excelApp.ActiveSheet.Range(sFinalColumn + ":" + sFinalColumn).Font.Bold = 1;




//Now we need to go through and update the subtotal formula and formatting
progressBar.setTitle("Adding subtotal");
progressBar.setMessage("Adding subtotal");
progressBar.setProgress(aRowSubtotal.length, 1);
progressBar.setPos(1);

for(var i=0; i<aRowSubtotal.length; i++){
	
	var oRowDetails = aRowSubtotal[i];
	
	progressBar.updateProgress(1);
	progressBar.setMessage(oRowDetails.id + "-" + oRowDetails.name);
	
	//set the title
	excelApp.ActiveSheet.Cells(oRowDetails.end,1).value = oRowDetails.id;
	excelApp.ActiveSheet.Cells(oRowDetails.end,2).value = oRowDetails.name;
		
	//Create the first total formula
	excelApp.ActiveSheet.Cells(oRowDetails.end,3).formula = "=SUM(C" + oRowDetails.start + ":C" + (oRowDetails.end-1) + ")";
	
	//Lets copy it
	excelApp.ActiveSheet.Range("C" + oRowDetails.end).Copy;
	excelApp.ActiveSheet.Range("D" + oRowDetails.end + ":" + CWANZ_numToColumn(iFinalColumnIndex-1) + oRowDetails.end).Select;
	excelApp.ActiveSheet.Paste;
	
	//set a top border
	var oTopBorder = excelApp.ActiveSheet.Range("C" + oRowDetails.end + ":" + sFinalColumn + oRowDetails.end).Borders.Item(8);
	oTopBorder.lineStyle = 1 ;//xlContinuous 
	oTopBorder.Weight = 2; //xlThin
	
	//Lets Bold it
	excelApp.ActiveSheet.Range(oRowDetails.end + ":" + oRowDetails.end).Font.Bold = 1;
	
	//lets clear the next row
	excelApp.ActiveSheet.Range((oRowDetails.end + 1) + ":" +(oRowDetails.end + 1)).Clear;
}

//Lets autofit the column
excelApp.ActiveSheet.Columns("A:"+sFinalColumn).EntireColumn.AutoFit;

//Lets apply number format
excelApp.ActiveSheet.Columns("C:"+sFinalColumn).NumberFormat = "#,##0.00";

//Lets bold the header
excelApp.ActiveSheet.Range("1:1").Font.Bold = 1;

//Show the excel document
excelApp.visible = true;

//Complete Process
document.messageBox("Complete","Data Extraction Complete, please review the excel document",MESSAGE_OK);


function formatRowData(aDetails){
	var sText = "";
	var sDelim = "\t";
	
	for(var d=0; d<aDetails.length; d++){
		sText += aDetails[d] + sDelim;
	}
	
	return sText;
}


function processAccounts(oAccounts){
	var oAccountList = {};
	
	for(var acc = new Enumerator(oAccounts); !acc.atEnd(); acc.moveNext()){
		
		var account = acc.item();
		var entityAbbreviation = account.Entity.Abbreviation;
		var ayBalance = account.Balances.ReportBalance(0); //Current year balance
		var py1Balance = account.Balances.ReportBalance(1); //Prior year balance
		
		//check for properties
		var sAccID = account.Id;
		
		if(!oAccountList[sAccID])
			oAccountList[sAccID] = {
				name : account.Name
			};
		
		if(!oAccountList[sAccID][entityAbbreviation])
			oAccountList[sAccID][entityAbbreviation] = {
				current: ayBalance,
				prior: py1Balance
			};	
	}
	
	return oAccountList;
}

/**
 * CWANZ_numToColumn() Converts numeric into alphabetical column. Max is 676 columns. <p>
 * The functions will convert the number by dividing with 26, which is the number of the alphabet
 * and then try to build the string to represent the column index. <p>
 * The limit of 676 columns is from the column ID A - ZZ.
 * @param  {Integer} 	iNumber the numeric column index to comvert
 * @return {String}		The alphabetical reprensetation of the column
 * @version 1.0 - 09/08/2013
 */
function CWANZ_numToColumn(iNumber)
{
	var ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	//Function Protection
	if(iNumber <=0 || iNumber >676)
		return;
		
	//First calculate the Level to figure out the first level of the alphabet
	var sColumn = "";
	while(iNumber > 26)
	{
		//Are we the last digit AZ
		if(iNumber % 26 == 0)
		{
			sColumn = ALPHABET.charAt(Math.floor(iNumber / 26) - 1);
			iNumber = 26;	 
		}
		else
		{
			//Calculate the first digit
			sColumn = ALPHABET.charAt(Math.floor(iNumber / 26));
			
			//Update iNumber
			iNumber = iNumber % 26;		
		}
	}
	
	//Add the last digit to the column
	sColumn = sColumn + ALPHABET.charAt(iNumber);
	
	//Return the completed alphabet
	return sColumn;
};//CWANZ_numToColumn()




