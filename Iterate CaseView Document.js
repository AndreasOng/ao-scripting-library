clear();
var progressBar 	= document.createProgressBar("Initialising",1,1,1,"Initialising");

var FSO 			=  new ActiveXObject("Scripting.FileSystemObject"); 
enumerateFolders(document.cell("PATH").value);

function enumerateFolders(sNewPath)
{
	var oStartFolder = FSO.GetFolder(sNewPath);
	if(oStartFolder)
	{		
		//Enumerate sub folders
		for(var fe = new Enumerator(oStartFolder.SubFolders); !fe.atEnd(); fe.moveNext())
		{
			var oFolder = fe.item();
			var sFolderName = "" + oFolder;
			
			progressBar.setMessage(sFolderName);
			
			if(progressBar.getCancelled())
				exitScript();
			
			processCaseViewFile(oFolder);
			
			//Process the next folder
			enumerateFolders(sFolderName);
		}
	}
}

function processCaseViewFile(oFolder)
{
	for(var f = new Enumerator(oFolder.files); !f.atEnd(); f.moveNext())
	{
		var sFile = f.item() + "";
		if(sFile.substring(sFile.length - 4, sFile.length) == ".cvw")
		{
            print(sFile);
            checkCaseViewFile(sFile);
            
            print("");
            print("");
		}
	}
}

function checkCaseViewFile(sFilePath){
    var oDoc = Application.openDocument(sFilePath);
    if(oDoc){
        for(var p=1; p<=oDoc.paraCount ; p++){

            var oPara = oDoc.para(p);
            if(oPara){
    
                //Check the style
                if(oPara.getStyleName().substring(0,7) === "Nt Text"){
    
                    //check for the menu DrilldownTextPara
                    if(!oPara.rightClickMenu){
                        print("Para Index " + oPara.index() + ": Missing right click menu");
                        print(oPara.getText());
                        print("");
                    }
    
                    if(!oPara.propExists("STYLE_LEVEL_CHANGEABLE")){
                        print("Para Index " + oPara.index() + ": Missing property STYLE_LEVEL_CHANGEABLE");
                        print(oPara.getText());
                        print("");
                    }
                }
            }
        }
        oDoc.close(0);
    }
}

function alert(sText){
    document.messageBox("Alert", sText, MESSAGE_OK);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}