/**
 * Reportable Item Script testing
 * https://bugs.caseware.com/browse/AS-1249
 */
clear();

 //Access the Reportable item object
 var RIControl = new cwReportableItem();

 //get all reportable Item ID
 //this function will return an array
 //["RI668350", "RI755536"]
 var aRI_ID = RIControl.GetCurrentReportableItemList();

 //Iterate through the control IDs
 for(var i=0; i<aRI_ID.length; i++){

    print("Reportable Item ID: " + aRI_ID[i]);

    //Check if the control exists
    if(RIControl.IsReportableItemExist(aRI_ID[i])){

        //Access the control object
        var objRRPT = new cwReportableItem(aRI_ID[i]);
        if(objRRPT){

            //Access the control id
            print("Loading control complete. ID = " + objRRPT.GetID());

            //Define the properties that we know
            var aRIProperties = [
                //IDENTIFIER
                //MGNTRESPOND   // Management Response
                //RFD           //Roll Forward
                //OTHER         //Other reportable item flag
                "NAME",         // Get Reportable Item name
                "OBS_OR_WEAK",  // Observation / weakness
                "WEAK",         // Weakness
                "STATUS_OPT",   // Status
                "ISSIGNEDOFF",  // Is Sign off
                "SIGNOFFBY",    // Sign off By
                "SIGNOFFDATE",  // Sign off Date
                "YEARIDENT",    // Years identified
                "CLASS_OPT",    // Classification
                "RATING_OPT",   // Rating
                "GROUPING_OPT", // Grouping
                "REASON"       // Reason
            ];

            //Iterate and print the properties
            for(var p=0; p<aRIProperties.length ; p++){
                print(aRIProperties[p] + " = " + objRRPT.GetReportableItemProp(aRIProperties[p], aRI_ID[i]));
            }

            //Recommendations
            var value = objRRPT.GetRecommendations(aRI_ID[i]);
            print("Recommendations: " + JSON.stringify(value));

        }

    }
 }

 function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}
