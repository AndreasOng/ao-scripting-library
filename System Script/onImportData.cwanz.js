/**
 * Copy key data from the CV database of the source client file
 * @event	OnImportData
 * @param	{string}	importType
 * @param	{string}	importFile
 * 
 * Change log
 * 20.10.2015	Added Repost() to fix the NetInc Issue
 * 22.04.2016	Updated the protection for the version check
 * 14.09.2016 	Removed the version protection, as it seems like we don't need to do it
 */
function OnImportData(importType, importFile)
{
	
	if(importType == "import.caseware"){

		//Query the file's template ID
		var sTemplateID = ClientVersionInfo.SourceTemplateID;	//Template name
		var WPGTEMPDIR = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGTEMPDIR"); //Template directory
		var WPGVER = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGVER"); //Template version
		
		//Protection Level 1. Protection Check if there are values in the versions
		if(sTemplateID && WPGTEMPDIR && WPGVER)
		{
			//Protection Level 2. Protection, check the versions
			//This to make sure we have the right scripts for the right versions
			if(sTemplateID == "Audit System" && WPGTEMPDIR == "Audit System")
			{
			
				//21 Jan 2014
				//Move all our custom script to another function to allow
				//better future changes when CWI Releases a new script
				CWANZ_Functions(importFile);
			}
		}
		
		//to fix the NETINC issue
		Repost();
	}
}


function CWANZ_Functions(i_sSourceFilePath)
{
	
	/******** ROLL FORWARD FUNCTIONALITY *********************************/
	/**** Offer roll forward option if the source fle has trial balance **/
	/********************************************************************/
	//Check if accounts exist, offer roll forward option
	var e = new Enumerator(Accounts);
	if (!e.atEnd()) { // Accounts exist, so ask if year end close should be perfomed
		if (MessageBox ("Year End Close", "Do you wish to perform a year end close on the existing data?", MESSAGE_YESNO) == MESSAGE_RESULT_YES) {
			var yec = YearEndClose;
			yec.UpdatePriorYearBalanceData = 1;
			yec.UpdateCaseViewRollForwardCells = 0;
			yec.DestinationFile = Client;
			yec.DoYearEndClose();
		}
	}
		
	/********* COPY THE DIRECTORS NAMES FROM IFRS *********************/
	/**** The script is copied directly from IFRS onImport Script *****/
	/******************************************************************/
	// Copy all external data that is inside
	// GROUP: ORIGINAL
	// FORM: DIRREPT
	var /** @type string */ sClientPath = i_sSourceFilePath.replace(/\.ac/i, "CV.dbf");
	var /** @type CWCaseViewData */ oDataSet = SystemCaseViewDataSet(sClientPath, "ORIGINAL", "DIRREPT", "*");
	var /** @type Enumerator */ oCvRecords = new Enumerator(oDataSet);
	
	oCvRecords.moveFirst();
	while (!oCvRecords.atEnd()) {
		var x = oCvRecords.item();
		// Need to confirm that this is a valid match.
		
		var /** @type number */ nType = oDataSet.GetGroupFormIdType(x.Group, x.Form, x.Id);
		var /** @type number */ nOvr = oDataSet.GetGroupFormIdOvr(x.Group, x.Form, x.Id);
		
		// don't overwrite any existing, non-blank records brought in from the new template
		if (!CaseViewData.ExistsGroupFormId(x.Group, x.Form, x.Id) || CaseViewData.DataGroupFormId(x.Group, x.Form, x.Id) == "") {
			CaseViewData.SetGroupFormIdData(x.Group, x.Form, x.Id, nType, nOvr, x.Data);
		}
		
		oCvRecords.moveNext();
	}
}