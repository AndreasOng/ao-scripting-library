/**
 * This OnFileOpen will be executed on both FinancialsIFRS and Audit System files when it is loaded.
 * The purpose of this script is to perform a once off check on the mapping structure and perform a copy
 * template if required.
 * 
 * The script is searching for the SourceInfo/GUID field in the Mapping - Tags (access the MP database to view)
 *
 * The outcome is as follows
 * 1. File is OK
 * 2. Mapping is corrupted and fixed
 * 3. Mapping is corrupted and client choose to ignore it
 *
 * Once any of the events above has been completed, we will set a flag on the file so that the script is no longer executed
 *
 * Change Log
 * V 1.9 - added protection for Locked down file
 * V 1.10 - Copied the CHANGE MAPPING function from the patch to update the mapping properties
 * V 1.11 - Removed the automatic update, and only alert the user
 * V 1.12 - Removed the call to remove duplicate mapping and added extra protection to avoid false positive
 
 * @author CaseWare Australia and New Zealand
 * @method OnFileNew()
 * @version 1.9 - 07/08/2015
 */
function OnFileOpen() 
{
	//Query the file's template ID
	var sTemplateID = ClientVersionInfo.SourceTemplateID;

	//Check if file is not locked down
	if(IsLockedDown)
		return;
	
	//Make sure that this is not the template
	if(!sTemplateID || FilePath.indexOf("\\Template")>=0 || FilePath.indexOf("\\Document Library")>=0)
		return;
		
	//Filter if the check has been performed
	if(CaseViewData.DataGroupFormId("CWANZ","FLAG","onFileNew"))
		return;
		
	//Prepare the error message
	var MSG = { log:[ "Update Mapping Source"]};
	
	//Variables for duplicate mapping issue
	var oDupId = {
		"1.5.2.900.111" : 0,
		"1.5.2.900.112" : 0
	}

	var iFlag = 1;
	
	try{
	
		//Query the file's template ID
		var sTemplateID = ClientVersionInfo.SourceTemplateID;						//Template name
		var sMappingID = ClientVersionInfo.SourceMappingID ;						//Where the mapping comes from
		var impacted = false;														//Missing Source Info Code
		var result = false;															//Dialog Box variable
		
		//Additional function to fix the duplicate mapping points
		function checkMapping()
		{
			try{
			//Check mapping
				for(var e = new Enumerator(Mappings); !e.atEnd(); e.moveNext())
				{
					var oMapping = e.item();
					
					//Scan for source info
					//v 1.12 added extra condition so it will check mapping numbers only (skip blank ID and blank Name)
					if (!oMapping.ItemProperty('SourceInfo/GUID') && oMapping.Id !== "" && oMapping.Name !== "") {
								impacted = true;
					}
					
					//Scan for duplicate mapping, use the oDupId
					if(oDupId.hasOwnProperty(oMapping.Id)){
						oDupId[oMapping.Id]++;
					}
				}
			}
			catch(err){
				MSG.log.push("Error: checkMapping() failed");
				MSG.log.push(err.message);
			}
		}
		
		function removeDuplicates(sMapNo, aSettings){
			try{
				if(oDupId[sMapNo] === 2){
					//Remove the first instance
					Mappings.remove(sMapNo);
					
					MSG.log.push(sMapNo + " duplicate has removed");
				}
			}
			catch(err){
				MSG.log.push("Error: removeDuplicates() failed");
				MSG.log.push("sMapNo: " + sMapNo);
				MSG.log.push("aSettings: " + aSettings);
				MSG.log.push(err.message);
			}
		}
		
		function TASKTECH_CHANGE_MAPPING()// CWANZ - 24 August 2015
		{
			try{
				//modify calculation and descriptions
				var oInfoArray2 = new Array(
				new Array("1.5.2.900.111","Financial guarantee contracts","Financial guarantee contracts",0,1,2,"1.5.2.400.100.100.550.000.00010.000,1.5.2.400.100.100.550.000.00020.000","","","","","IGNORE","",""),
				new Array("1.5.2.900.112","Deferred income","Deferred income",0,1,2,"1.5.2.440.100.000.100.000.00000.000,1.5.2.440.100.000.150.000.00000.000,1.5.2.440.100.000.900.000.00000.000","","","","","IGNORE","","")
				);


				//looping for map numbers modification - calculation
				var oMappings	= Mappings;

				//	var oProgBar	= document.createProgressBar("Changing Mapping Entries", oInfoArray.length, 1, 0);
				for (var j = 0; j < oInfoArray2.length; j++)
				{
				//		oProgBar.updateProgress(1);
					var sMapNo		= oInfoArray2[j][0];
					var sName	= oInfoArray2[j][1];
					var sExDescription	= oInfoArray2[j][2];
					var sAccountType	= oInfoArray2[j][3];
					var sSignType = oInfoArray2[j][4];
					var sBehaviourType = oInfoArray2[j][5];
					var sCalculation = oInfoArray2[j][6];
					//var sLS_SMSF = oInfoArray2[j][8];
					var sLS		= oInfoArray2[j][7];
					var sClassType = oInfoArray2[j][13]; 
					//var sLSFlip		= oInfoArray2[j][x];
					//var sTitle		= oInfoArray2[j][x];
					//var sMappingFlip		= oInfoArray2[j][7];
					
					var oMapping = oMappings.Get(sMapNo); 
					if (oMapping)
					{
						
						sName == "" ? null : oMapping.Name = sName;
						sExDescription === "" ? null : oMapping.ExDescription = sExDescription;
						oMapping.AccountType = sAccountType; //BS
						oMapping.SignType = sSignType; //CREDIT
						oMapping.BehaviourType = sBehaviourType; //CALCULATED
						sCalculation === "" ? null : oMapping.Calculation = sCalculation;
									
						//if(TASKTECH_CHECK_GROUPING(2,sLS_SMSF))
						//		oMapping.Grouping(2) = sLS_SMSF;
						
						//if(TASKTECH_CHECK_GROUPING(1,sLS))
						sLS === "" ? null : oMapping.Grouping(1) = sLS;
						// change a new record
					
						//if (sTitle != "" && sTitle != undefined)
						//{
						//oMapping.set("Title", sTitle);
						//}		
					
						//if (sClassType != "" && sClassType != undefined)
						//{			
						//	oMapping.ClassType=getClassType(sClassType);
						//}
						//if (sLSFlip != "" && sLSFlip != undefined)
						//{
						//	oMapping.set("LSFLIP", sLsFlip);
						//}
						
						/**************** SET MAPPING FLIP **************************/
						/*oMapping.FlipType = 0; 	//Individual Flip
						if(sMappingFlip === "" || oMappings.Get(sMappingFlip))
						{
							oMapping.MappingFlip = sMappingFlip;	//Assign flip mapping
						}*/
						
						MSG.log.push(sMapNo + " properties has been set");	
					}
				}
			}
			catch(err){
				MSG.log.push("ERROR Unable to set mapping properties");
				MSG.log.push(err.message);
			}

			//oProgBar.destroyProgressBar();
		}
		
		//Template ID existed, so let's perform the operation
		if(sTemplateID && (sTemplateID === "Audit System" || sTemplateID === "FinancialsIFRS")){
		
			//Variables required for testing			
			var template_guid = ClientVersionInfo.ComponentSourceTemplateGUID(tucMap);		//Get the template GUID
			
			//Found template GUID - Let's process it
			if(template_guid){
				
				//Check the mapping structure
				checkMapping();
				
				//Silently removes the duplicate mapping points
				//removeDuplicates("1.5.2.900.111");
				//removeDuplicates("1.5.2.900.112");
				//TASKTECH_CHANGE_MAPPING();
								
				//Looks like we have found an issue
				if (impacted) {
						
					//Ask the user whether they want to rectify it
					result = MessageBox(
						"Mapping Source Missing",
						"Mapping source data is missing.\nPlease refer to FAQ 532 to update the mapping structure.",
						MESSAGE_OK
					);	
					
					//FinancialsIFRS14 update - only alerts
					MSG.log.push("Mapping structure corruption detected, alert shown once");					
				}
				else{
					MSG.log.push("File is OK");
				}
			}
		}
	}
	catch(err2){
		//Catch an error, do something
		MSG.log.push("General Error Encountered");
		MSG.log.push(MSG.message);
	}
	
	MSG.log.push("Process Completed");
	
	//refresh file
	Repost();
	
	//Store messages to the CaseView Data
	CaseViewData.DataGroupFormId("CWANZ","FLAG","onFileNew") = iFlag;
	CaseViewData.DataGroupFormId("CWANZ","LOG","onFileNew") = JSON.stringify(MSG);
}






