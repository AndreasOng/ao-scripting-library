/**
 * Copy key data from the CV database of the source client file
 * @event	OnImportData
 * @param	{string}	importType
 * @param	{string}	importFile
 * 
 * Change log
 * 19 06 19		New Version for PSAT template, The code is copied from the FinancialsIFRS code to remap to IFRS
 *				It will require a QAO text file to work
 */
function OnImportData(importType, importFile)
{
	
	if(importType == "import.caseware"){

		//Query the file's template ID
		var sTemplateID = ClientVersionInfo.SourceTemplateID;	//Template name
		var WPGTEMPDIR = CaseViewData.DataGroupFormId("WPG_TEM_DATA","PUBLICSECTOR","WPGTEMPDIR"); //Template directory
		var WPGVER = CaseViewData.DataGroupFormId("WPG_TEM_DATA","PUBLICSECTOR","WPGVER"); //Template version
		
		//Protection Level 1. Protection Check if there are values in the versions
		if(sTemplateID && WPGTEMPDIR && WPGVER)
		{
			//Protection Level 2. Protection, check the versions
			//This to make sure we have the right scripts for the right versions
			if(sTemplateID == "Public Sector" && WPGTEMPDIR == "PUBLICSECTOR")
			{
				if (MessageBox ("Remap Accounts", "Remap the trial balance to 'PSAT' mapping?", MESSAGE_YESNO) == MESSAGE_RESULT_YES){
					CWANZ_Functions(importFile);
				}	
			}
		}
		
		//to fix the NETINC issue
		Repost();
	}
}


function CWANZ_Functions(i_sSourceFilePath)
{
	var sRemapFile = ScriptPath + "QAO.txt";
	
	// build list of mapping numbers that are to be remapped
	var remap = new oRemap(sRemapFile);

	// go through account database remapping or removing
	for (var e = new Enumerator(Accounts);!e.atEnd();e.moveNext()) {
		var account= e.item();
		var mapNo = trimall(account.Groupings.mapping);
		if (mapNo){	
			account.Groupings.Mapping = remap.dict.Exists(mapNo) ? trimall(remap.dict.Item(mapNo)) : "";
			//messagebox(mapNo,remap.dict.Exists(mapNo) + "\n" + account.Groupings.Mapping ,1);
		}
		var mapNoFlip = trimall(account.Groupings.MappingFlip);
		if (mapNoFlip) account.Groupings.MappingFlip = remap.dict.Exists(mapNoFlip) ? trimall(remap.dict.Item(mapNoFlip)) : "";
	}
}

//////////////////////////////////////////////////////////////////////////////
// Object oRemap(sTextFile)
//
// Object containing dictionary of remap items
//
// Parameters:
//  sTextFile - name of text file containing mapping numbers and their remaps

function oRemap(sTextFile)
{
    try
    {
        if(!sTextFile || sTextFile.constructor != String)
            throw "Text file containing mappings and their remaps must be supplied";

        // Local Properties

        // External Properties
        this.dict = new ActiveXObject("Scripting.Dictionary");
    
        // Local Methods
        this.ParseLine = oRemap_ParseLine;

        // External Methods

        // init
        var ForReading = 1 ;
        var ForWriting = 2 ;
        var TristateUseDefault = -2 ;

        var fso = new ActiveXObject( "Scripting.FileSystemObject" ) ;
        var fin = fso.GetFile( sTextFile ) ;
        var ts = fin.OpenAsTextStream( ForReading, TristateUseDefault ) ;
        while( !ts.AtEndOfStream )
        {
            var oLine = this.ParseLine(ts.ReadLine()) ;
            if(!this.dict.Exists(oLine.map))
                this.dict.Add(oLine.map, oLine.remap);
        }
    }

    catch( e )
    {
        throw "Function oRemap() - Unable to read mapping file";
    }
}

function oRemap_ParseLine(sText)
{
	var line = new Object();

    try
    {
        if(!sText || sText.constructor != String)
            throw "Line from text file must be provided";
            
		var tabPos = sText.indexOf("\t");
        if (tabPos != -1) {
			line.map = trimall(sText.substr(0, tabPos));
			line.remap = sText.substr(tabPos + 1);
		}
		else {
			line.map = sText;
			line.remap = "";
		}

	return line;
    }

    catch( e )
    {
        throw "Function oRemap_ParseLine(): " + e + mapNo;
    }
}

//////////////////////////////////////////////////////////////////////////////

function trimall( str )
{
    // remove all spaces

    var retstr = "" ;

    var pos = 0 ;
    for (var pos = 0; pos < str.length; pos++ ) {
	if (str.charAt( pos ) != " ")
		retstr += str.charAt( pos );
    }
 
    return retstr.replace(/\t/g, "") ;
}

//////////////////////////////////////////////////////////////////////////////