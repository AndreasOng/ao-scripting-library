/*
 * Those are the information what you required:
 *
 * OnDeleteDocument(id, fromCOM) { }
 *
 * Parameters
 * id - the id of the document being deleted 
 * fromCOM: If deleteion is from COM
 *
 * This script is created to cater for a client request where they would like to protect any documents that has been signed off from being deleted
 * This feature is not supported in CaseWare Advanced Security, so this is why we need to do this script
 * To use this script, all you need to do is copy this into C:\Program Files (x86)\CaseWare\Script  
 *
 * This is to address Zoho 16528
 * https://support.caseware.com.au/support/caseware/ShowHomePage.do#Cases/dv/3ba30874d22f3cb86dd75790a5c532fa4b4f3d0ad4321831
 * https://bugs.caseware.com/browse/DIST-6475
 * 
 * Author: AO
 * @Version 1.0 - Initial Version
 */
 
 
function OnDeleteDocument(id, fromCOM) {
	
	//Get the number of roles that the engagement file is using
	var iNoRoles = ClientOptions.NumberRoles; 
	
	//Check the documents against the document managers
	var oDoc = Documents.Get(id);
	if(oDoc){
		
		//check the signed off for the relevant roles
		for(var i=1; i<=iNoRoles; i++){
			
			//If we have any sign off info, cancel the operation
			if(oDoc.SignoffDate(i) || oDoc.SignoffInitials(i)){
				
				//Do not delete the document
				//alert(iNoRoles + "id: " + id + "   from COM: " + fromCOM);
				return false;
			}
		}		
	}
	
	//Allow document deletion
	//alert("DELETED " + iNoRoles + "id: " + id + "   from COM: " + fromCOM);
	return true;
}