/**
 * This function is the gateway, it will be executed when the file is created
 * This functions need to be deployed to C:\Program Files (x86)\script
 * This function has protection that it will only run on Audit System
 * @author CaseWare Australia and New Zealand
 * @method OnFileNew()
 * @version 8.5 - 10 09 2018
 * Version 8.0 - Notes
 * Updated the remove section to remove only the industry toolbar. also updated the condition to match only the version numbers
 * as Tim changes the version on the release date, and enable the try and catch section on the copy financials 
 * Version 8.1 - Removed Audit Version
 * Version 8.2 - Allow for all profile
 * Version 8.3 - Added sub template protection
 * Version 8.4 - Removed combo file settings from profile 3 - Micro Audit
 * Version 8.5 - Added functionality to remove mapping from NETINC account in profile 3 - Micro Audit
 * Version 8.6 - Added functionality for Q documents
 */
function OnFileNew() 
{
	//Prepare the error message
	var MSG = { error:[ "OnFileNew()"]};
	
	//Query the file's template ID
	var sTemplateID = ClientVersionInfo.SourceTemplateID;	//Template name
	var WPGTEMPDIR = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGTEMPDIR"); //Template directory
	var WPGVER = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGVER"); //Template version
	
	//Protection Level 1. Protection Check if there are values in the versions
	if(sTemplateID && WPGTEMPDIR && WPGVER)
	{
		//Protection Level 2. Protection, check the versions
		//This to make sure we have the right scripts for the right versions
		//messageBox(sTemplateID,WPGTEMPDIR + "\n" + WPGVER,MESSAGE_OK);
		if(sTemplateID == "Audit System" && WPGTEMPDIR == "Audit System")
		{
			//Protection Level 3. Protection based on the Profile
			//Get the profile index
			// 1. Australia
			// 2. New Zealand
			// 3. SMSF ---- DISABLED
			// 4. Review
			// 5. One Form Australia
			// 6. OneForm New Zealand
			// to beupdated
			var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","")
				
			if(iProfile)
			{
				//NZ Dates
				if(iProfile == 2 || iProfile == 6)
				{
					CWANZ_setNZDates();
				}
				
				//Clean up toolbars
				CWANZ_CleanUpToolbar(iProfile);
				
				//messageBox("iProfile",iProfile,MESSAGE_OK);
				//Australia copy financials
				//Changed for all profiles
				//version 8.4 changes
				if(iProfile != 3)
				{
					//var CWAPP2 = new ActiveXObject("CaseWare.Application");
					CWANZ_CopyFinancials(Client);
				}


				CWANZ_profile3(iProfile);
				CWANZ_setQDocument();

				//Refresh WP layout
				UpdateUI();	
			}
			else
			{
				MSG.error.push("No profile detected");
			}
		}
		else
		{
			//Version 8.3 update - sub template protection
			//This tile is not created from Audit System template
			if(WPGTEMPDIR == "Audit System")
			{
				//the template directory says audit system, but the source ID is not.
				//So need to alert the users
				var sMsg = "This file is NOT based on Audit System template.\n"
				sMsg += "This operation is not supported by CaseWare Australia & New Zealand.\n"
				sMsg += "Please contact CaseWare support for assistance.";
				
				messageBox("Incorrect Template", sMsg, MESSAGE_OK);
			}
						
			MSG.error.push("Not an Audit System Engagement File");
			MSG.error.push("Source Template: " + sTemplateID);
			MSG.error.push("Template Directory: " + WPGTEMPDIR);
		}
	}
	else
	{
		MSG.error.push("Missing sTemplateID && WPGTEMPDIR && WPGVER");
	}
	

	//Write messages
	CaseViewData.DataGroupFormId("CWANZ","LOG","onFileNew") = JSON.stringify(MSG);
}

/**
 * This function will perform operations related to profile 3.
 * At the moment it will remove the assigned mapping and groupings from the NETINC account
 * @param	iProfile		Integer		The profile index to process.
 * @returns	none
 * @version 1.0
 * ChangeLog
 * 1.0		10 09 2018	Version 1 implemented on script version 8.5
 */
function CWANZ_profile3(iProfile){
	//Check for Profile 3
	if(iProfile == 3){

		//Access the account NETINC
		var NETINC_Acc = "NETINC";
		var oAccount = Accounts.get(NETINC_Acc);

		if(oAccount){
			//Remove from Mapping
			oAccount.Groupings.Mapping = "";
			oAccount.Groupings.MappingFlip = "";
			
			//Remove from Grouping
			for(var i=1 ; i<11; i++){
				oAccount.Groupings.Grouping(i) = "";
				oAccount.Groupings.GroupingFlip(i) = "";
			}
		}
	}
}

/**
 * This function will set the Q document. Currently each profile will have a separate Q document
 * We need to rename them from Q1 to Q so that the button to update metadata will work
 * @param	none
 * @returns	none
 * @version 1.0
 * ChangeLog
 * 1.0		10 09 2018	Version 1 implemented on script version 8.5
 */
function CWANZ_setQDocument(){
	//Step 1, check if Q document exist
	var QDOCID = "Q";
	var oQDoc = Documents.get(QDOCID);

	if(!oQDoc){
		//Find alternative document
		var aDocList = ["Q1","Q2","Q3","Q4","Q5"];
		for(var a=0; a<aDocList.length; a++){
			oQDoc = Documents.get(aDocList[a]);
			if(oQDoc){
				//Rename the document
				oQDoc.Id = QDOCID;

				//Complete the function
				return;
			}
		}
	}else{
		//Do Nothing, Q doc exists
	}
}

/**
 * This function will set the proper year end for NZ entities
 * It will take the current engagement year, and modify it to 31 march
 * @author CaseWare Australia and New Zealand
 * @method CWANZ_setNZDates()
 * @version 1.0 - 16/07/2014
 */
function CWANZ_setNZDates()
{
	//Get the current system date
	var dCurDate = new Date(ClientProfile.YearEndDate);


	//Adjust the date to 31 March, keep the year
	dCurDate.setMonth(2);	//Set month to March (0 based index)
	dCurDate.setDate(31);	//Set date to 31
	
	//Update the client profile year end date
	ClientProfile.YearEndDate = dCurDate.getVarDate();

	//Adjust the date to 1 April last year
	dCurDate.setYear(dCurDate.getYear() - 1);	//Set the year 1 year back
	dCurDate.setDate(dCurDate.getDate() + 1);	//Move the date 1 day forward
	
	//Set the starting date to the client profile
	ClientProfile.YearBeginDate = dCurDate.getVarDate();
}

/**
 * This function will go through the template toolbar on the newly created file
 * and remove any toolbar that is no longer required for the selected profile.
 * @author CaseWare Australia and New Zealand
 * @method CWANZ_CleanUpToolbar()
 * @param {Integer}	iProfile 	The selected profile index
 * @version 1.0 - 16/07/2014
 */
function CWANZ_CleanUpToolbar(iProfile)
{
	//Remove the Industry Toolbar
	if(TemplateToolbarButtons.Get("INDUSTRY"))
	{
		TemplateToolbarButtons.Remove("INDUSTRY");
	}

	return;

	//Skipping this section, as the template is performing wel in 2015
	
	if(iProfile >0)
	{
		//Prepare the Controls
		var aToolBarID = new Array("",	"AO",	"IMPORT",	"MAPPING",	"CLNT",	"SNAP",	"OPT",	"OMAT",	"FSA",	"OCR",	"RRPT",	"CRPT",	"MRPT",	"ASA",	"MANUAL",	"UPDATE",	"METADATA",	"PAGESETUP",	"MULTIFS",	"GENWPG",	"GENCKL");
		var aToolbarSettings = new Array(
									//(0,1,2,3,4,5,6)
			/* ZERO */ 		new Array(1, 1,1,1,1,1,1),						
			/* AO */ 		new Array(1, 1,1,1,1,1,1),
			/* IMPORT */	new Array(1, 0,0,1,0,0,0),
			/* MAPPING */	new Array(1, 1,1,1,1,1,1),
			/* CLNT */		new Array(1, 0,0,1,0,0,0),
			/* SNAP */		new Array(1, 0,0,1,0,0,0),
			/* OPT */		new Array(1, 1,1,1,1,1,1),
			/* OMAT */		new Array(1, 1,1,0,1,1,1),
			/* FSA */		new Array(1, 1,1,0,1,1,1),
			/* OCR */		new Array(1, 1,1,1,1,1,1),
			/* RRPT */		new Array(1, 1,1,0,1,1,1),
			/* CRPT */		new Array(1, 1,1,0,0,1,1),
			/* MRPT */		new Array(1, 1,1,0,1,1,1),
			/* ASA */		new Array(1, 1,1,1,0,0,0),
			/* MANUAL */	new Array(1, 0,0,0,0,0,0),
			/* UPDATE */	new Array(1, 1,1,1,1,1,1),
			/* METADATA */	new Array(1, 1,1,0,0,0,0),
			/* PAGESETUP */	new Array(1, 0,0,0,0,0,0),
			/* MULTIFS */	new Array(1, 1,0,0,0,1,0),
			/* GENWPG */	new Array(1, 1,1,1,1,1,1),
			/* GENCKL */	new Array(1, 1,1,1,1,1,1)
		);
		
		//Go through the array to 
		for(var i=1; i<aToolBarID.length; i++)
		{
			//Check whether we need to keep the toolbar
			if(aToolbarSettings[i][iProfile] == 0)
			{
				//Remove the toolbar
				if(TemplateToolbarButtons.Get(aToolBarID[i]))
				{
					TemplateToolbarButtons.Remove(aToolBarID[i]);
				}
			}
		}
		
		//Remove the Industry Toolbar
		if(TemplateToolbarButtons.Get("INDUSTRY"))
		{
			TemplateToolbarButtons.Remove("INDUSTRY");
		}
	}
}

/**
 * This function will trigger a copy template functions to copy the Financials documents
 * from the IFRS template to the audit template
 * @author CaseWare Australia and New Zealand
 * @method CWANZ_CopyFinancials()
 * @param {CaseWareClient}	oClient		The current CaseWare Client Object
 * @version 1.0 - 09/08/2013
 */
function CWANZ_CopyFinancials(oClient) {
	try 
	{	
		var /** @type CaseWare **/			CWAPP;			//The CaseWare Object
		var /** @type CaseWareClient **/ 	ocDest;			//The destination client
		var /** @type CaseWareClient **/ 	ocSource;		//The source client
		var /** @type String **/ 			sIFRSPath;		//The path to IFRS Template
		var /** @type CWTemplateInfos **/	oTemplates;		//The template list information
		var /** @type CWCopyTempalate **/	oCopy;			//The Copy Template Object
		var /** @type Object **/			oDocList;		//The object listing all the documents to copy
		
		//Specify Which Document ID we need to copy accross
		oDocList = {
			"FSIFRS":1,		//Financial statements
			"SDD"	:1,		//Segment definition
			"SRW"	:1,		//Segment reporting worksheet
			"5E"	:1,		//Statement of cash flows worksheet
			"TWS"	:1,		//AASB 112 Income Taxes - Worksheet
			"5Q"	:1,		//Statement of cash flows worksheet (quarterly)
			"RPS"	:1,		//Rental Property Statement
			"DDBEQ"	:1,		//Engagement panel equation
			"VR"	:1,		//Version Information
			"CQ"	:1,		//Firm settings
			"CL"	:1,		//Firm financial options
			"1B"	:1,		//Formatting properties
			"DISTIF":1		//Distributor Financials
			};
		
		//Set up connections to the CaseWare Application
		CWAPP = new ActiveXObject("CaseWare.Application");
		//messageBox("MSG 1","ONE",MESSAGE_OK);
		
		//Locate the IFRS Financials Template
		oTemplates = CWAPP.TemplateList;
		if(oTemplates)
		{
			//messageBox("MSG 2","TWO",MESSAGE_OK);
			for(var i=1;i<=oTemplates.Count;i++)
			{
				var oTmp = oTemplates.Item(i);
				if (oTmp)
				{
					if (oTmp.Name == "FinancialsIFRS")
					{
						sIFRSPath = oTmp.FilePath;
					}
				}
			}
			
			if(sIFRSPath)
			{
				if(messageBox("Create a combined engagement file","Do you want to create a combined audit and financials file?",MESSAGE_YESNO) != MESSAGE_RESULT_YES)
					return;
				
				//messageBox("Alert",sIFRSPath,MESSAGE_OK);
				
				//Load the CaseWare Files
				ocDest = oClient;
				ocSource = CWAPP.Clients.Open2(sIFRSPath + ".ac","","",0,5000);
				
				//Prepare the Copy Template Object
				oCopy = CWAPP.CopyTemplate;
				oCopy.ResetDefaults();
				oCopy.SourceFile = ocSource;
				oCopy.DestinationFile = ocDest;
				oCopy.CopyTrialBalance = false;
				oCopy.CopyMappings = false;
				oCopy.CopyRoles = false;
				oCopy.CopyCVExternalData = true;
				
				//Goes through the document list in the Copy Template so that we only copy required docs
				var e = new Enumerator(oCopy.CopyDocuments);
				for(e.moveFirst(); !e.atEnd(); e.moveNext())
				{
					//Grab a document item
					var oTDoc = e.item();
					//messageBox("Alert2",oTDoc.Name,MESSAGE_OK);
					
					//Document is a CaseView Document
					if (oTDoc.Type == 2)
					{
						//CaseView Doc
						//Check whether the document ID is on our list
						if (oDocList.hasOwnProperty(oTDoc.Id))
						{
							//Mark it as required
							oTDoc.CopyFlag = 1;		
							
							//Go though the parent folders and tick them as well
							// neeed to go to the very top
							//Protection required as when they reach the top it will generate an error
							try
							{
								//Read the parent
								var oParentFolder = oTDoc.Parent;
								while(oParentFolder)
								{
									//Copy the parent too
									oParentFolder.CopyFlag = 1;
									
									//Check for the parent of parent
									oParentFolder = oParentFolder.Parent;
								}
							}
							catch(err2)
							{
							
							}
							
						}
						else
						{
							//Not our document, ignore in the copy
							oTDoc.CopyFlag = 0;
						}
					}
					else
					{
						//Not a caseview document, ignore
						oTDoc.CopyFlag = 0;
					}
				}//Mark Documents
				
				
				//Perform the Copy Operations
				//Protection required to suppress the error message
				//Expected error: unable to exclusive access to the file
				//But it still copies
				try
				{
					oCopy.DoCopy();	
				}
				catch (err3)
				{
				
				}
					
				//Refresh the user interface
				CWAPP.UpdateUI();
				
				//Re-order the folder to the right location
				//Financial Reports folder need to sit under 4-300 Financial Statements
				var oFinancialReports = CWANZ_GetFolder(oClient,"Financial Reports");
				var o4300FinancialStatements = CWANZ_GetFolder(oClient,"4-300 - Financial Statements");
				var oDisclosureFolder = CWANZ_GetFolder(oClient,"Disclosure Checklist");
				if(oFinancialReports && o4300FinancialStatements)
				{
					oFinancialReports.Parent = o4300FinancialStatements;
					
					if(oDisclosureFolder)
					{
						oFinancialReports.SetPosition(ptLevelBefore,oDisclosureFolder);
					}
				}
				
				//The Control documents need to sit at the bottom
				var oFSControlFolder = CWANZ_GetFolder(oClient,"IFRS Control Documents (do not delete)");
				var oAuditControlFolder = CWANZ_GetFolder(oClient,"Audit System - Control Documents (do not delete)");
				if(oFSControlFolder && oAuditControlFolder)
				{
					oFSControlFolder.SetPosition(ptLevelAfter,oAuditControlFolder); //ptAfterBottom = 1
				}
				
				//Re-run this function to collapse all folders
				CWANZ_GetFolder(oClient,"");
			}
		}	
	}
	catch(err)
	{
	
	}					
}

/**
 * This function traverse through the document manager and look for the specified folder
 * If the folder is found, it will return the object model. 
 * If none is found, it will return null
 * It will set the folder's expand properties to false, so that all folders is colapsed
 * @author CaseWare Australia and New Zealand
 * @method CWANZ_GetFolder()
 * @param {CaseWareClient}	oClient		The current CaseWare Client Object
 * @param {String}			sFolderName	The folder name to look for
 * @return {CaseWareDocument}	The folder object or null
 * @version 1.0 - 09/08/2013
 */
function CWANZ_GetFolder(oClient, sFolderName)
{
	//Prepare an enumerator to go through all the documents
	var e = new Enumerator(oClient.Documents);
	for(e.moveFirst(); !e.atEnd(); e.moveNext())
	{
		//Read a document item
		var oTDoc = e.item();
		
		//Is it a folder = 0
		if(oTDoc.type == dtFolder)
		{
			//Set the expand option to false
			oTDoc.Expanded = false;
			
			//Check whether this is the folder that we are looking for
			if(oTDoc.Name == sFolderName)
			{
				//Return the folder object
				return oTDoc;
			}
		}
	}
	
	//Can't find anything, return null
	return null;
}

