/**
 * This OnFileOpen is specific to the Audit System
 *
 * V 1.0 - Fix the update issue from DIST-3452
 
 * @author CaseWare Australia and New Zealand
 * @method OnFileOpen()
 * @version 1.0 - 07/08/2015
 */
function OnFileOpen() 
{

	//Prepare the error message
	var MSG = { error:[ "Applying SCPPATH"]};
	
	//Query the file's template ID
	var sTemplateID = ClientVersionInfo.SourceTemplateID;	//Template name
	var WPGTEMPDIR = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGTEMPDIR"); //Template directory
	var WPGVER = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGVER"); //Template version
	
	//Protection Level 1. Protection Check if there are values in the versions
	if(sTemplateID && WPGTEMPDIR && WPGVER)
	{
		//Protection Level 2. Protection, check the versions
		//This to make sure we have the right scripts for the right versions
		//messageBox(sTemplateID,WPGTEMPDIR + "\n" + WPGVER,MESSAGE_OK);
		if(sTemplateID == "Audit System" && WPGTEMPDIR == "Audit System")
		{
			try{
				
				//DIST-3452
				CaseViewData.DataGroupFormId("","","SCPPATH") = "\\\\broadview\\template\\OurTemplate\\FinancialsIFRS\\Scripts\\FinancialsIFRS";
			}
			catch(err){
				MSG.error.push(err.message);
			}
			
		}
		
	}
	
	//Write messages
	CaseViewData.DataGroupFormId("CWANZ","LOG","onFileOpenAUD") = JSON.stringify(MSG);

}
