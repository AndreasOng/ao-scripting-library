/**
 * Constants
 * AUDIT 20
 */
var CWANZ_CONSTANTS = {
    TABLETYPE_CRPT  : "CRPT",
    TABLETYPE_MRPT  : "MRPT",
    CWANZ_TABLETYPE : "CWANZ_TABLETYPE",
    CWANZ_ID        : "CWANZ_ID",
    CWANZ_DISPLAY   : "CWANZ_DISPLAY",
    CWANZ_SHADE     : "CWANZ_SHADE",
    CWANZ_STYLE     : "CWANZ_STYLE"
}

 /**
  * AUDIT 20
  * CWANZ_TABLECONTROL Object will the the Object that will represent the table on the document
  * This object will also create either the Control / Reportable Item / Risk object
  * This object will be responsible to iterate through the data and populate the column
  * @Version 1.0
  * 
  * Change Log
  * 1.0     30 07 2018      Initial version
  */
 var CWANZ_TABLECONTROL = function(TABLE_Object){

    /* INTERNAL VARIABLE */
    var DATA_Object;                //The source data object

    /* INTERNAL FUNCTION */
    function init(){
        //Identify the table Object
        if(TABLE_Object.propExists(CWANZ_CONSTANTS.CWANZ_TABLETYPE)){
            //is it a CRPT table
            if(TABLE_Object.propGet(CWANZ_CONSTANTS.CWANZ_TABLETYPE) == CWANZ_CONSTANTS.TABLETYPE_CRPT){
                //Initialise connection to CRPT Object
                DATA_Object = new CWANZ_CRPT();
            }
            else if(TABLE_Object.propGet(CWANZ_CONSTANTS.CWANZ_TABLETYPE) == CWANZ_CONSTANTS.TABLETYPE_MRPT){
                //Initialise connection to CRPT Object
                DATA_Object = new CWANZ_MRPT();
            }
        }
    }

    function getStartRow(){
        var sSectionName = "CWANZ_TABLE_" + TABLE_Object.getLabel() + "_DATA";
        var oSection = document.sectionByName(sSectionName);
        if(oSection){
            return TABLE_Object.rowIndexFromParaIndex(oSection.firstParaIndex);
        }
        else{
            return 2;   //Default starting Row
        }
    }

    function expandTable(){
        var iAvailableRow = TABLE_Object.nRows() - getStartRow();   //The available row
        
        //Do we have enough rows
        if(iAvailableRow < DATA_Object.getCount()){
            //Expand the row using the last row as the source
            var iHowManyRows = DATA_Object.getCount() - iAvailableRow;
            TABLE_Object.insertRowAfter(TABLE_Object.nRows(),iHowManyRows,TABLE_Object.nRows()-1,1);
            document.recalculate(0);
        }
    }

    function readColumnSettings(sCustProp, sData){
		var /* @type String **/ sValue;
		var /* @type String **/ sKey;	
		
		if(sCustProp.indexOf(",")>= 0)
		{
			//Break down the elements 
			var /* @type Array **/ 	aValues = sCustProp.split(",");
			if(aValues)
			{
				for(var a=0; a<aValues.length; a++)
				{
					//Split the data to key and value
					sKey = aValues[a].substring(0,aValues[a].indexOf(":"));
					if(sData == sKey)
					{
						sValue = aValues[a].substring(aValues[a].indexOf(":")+1,aValues[a].length);
					}
				}
			}
		}
		else
		{
			sValue = sCustProp;
		}
	
		return sValue;
    }

    /* EXTERNAL FUNCTION */
    this.populateTable = function(){

        //Protection
        if(!DATA_Object)
            return;

        //Expand the table if required
        expandTable();

        //Go through every row and populate the column
        var iRow = getStartRow();
        for(DATA_Object.moveFirst(); !DATA_Object.atEnd(); DATA_Object.moveNext()){

            //Store the ID on the row custom property
            var oRow = TABLE_Object.getRow(iRow);
            oRow.propSet(CWANZ_CONSTANTS.CWANZ_ID, DATA_Object.getGUID());

            //Iterate through the table columns
            for(var c=1; c<=TABLE_Object.nColumns(); c++){

                //get a column
                var oColumn = TABLE_Object.getColumn(c);

                //Check if the CWANZ_ID property existed
                if(oColumn.propExists(CWANZ_CONSTANTS.CWANZ_ID)){

                    //get the cell that we are going to populate
                    var sTargetCell = TABLE_Object.getLabel() + "." + CWANZ_numToColumn(c) + iRow;
                    var oCell = document.cell(sTargetCell);
                    
                    if(oCell){
                        //get the data
                        var sData = DATA_Object.getValue(oColumn.propGet(CWANZ_CONSTANTS.CWANZ_ID));

                        //Data Protection
                        if(sData != null && sData != undefined){
                            //Check if there is a column settings for custom dsiplay settings
                            if(oColumn.propExists(CWANZ_CONSTANTS.CWANZ_DISPLAY)){
                                oCell.value = readColumnSettings(oColumn.propGet(CWANZ_CONSTANTS.CWANZ_DISPLAY),sData);
                            }
                            else{
                                oCell.value = sData;
                            }

                            //Check if we need to apply shading
                            //Apply the proper shading to the table
                            if(oColumn.propExists(CWANZ_CONSTANTS.CWANZ_SHADE))
                            {
                                var /** @type String **/ sShade = readColumnSettings(oColumn.propGet(CWANZ_CONSTANTS.CWANZ_SHADE),sData);
                                if(sShade)
                                {
                                    oCell.shadeType = SHADE_SOLID;
                                    oCell.shadeColor = sShade;
                                }
                            }

                            //Check if the user has specify the style sheet
                            if(oColumn.propExists(CWANZ_CONSTANTS.CWANZ_STYLE))
                            {
                                //Get the right style name
                                var /* @type String **/ sStyleName =  readColumnSettings(oColumn.propGet(CWANZ_CONSTANTS.CWANZ_STYLE),sData);
                            
                                if(sStyleName)
                                {
                                    //goes through all the paragraph and apply the style
                                    for(var p=TABLE_Object.cellFirstParaIndex(iRow,c); p<=TABLE_Object.cellLastParaIndex(iRow,c); p++)
                                    {
                                        var /* @type ICaseViewParagraph **/ oCellPara = document.para(p);
                                        oCellPara.applyStyle(sStyleName);
                                    }
                                }
                            }
                        }
                    }
                }   
            }

            //Increase the row index
            iRow++;
        }

        //Do we need to clean up the rest of the rows as a protection
        for( ; iRow <= TABLE_Object.nRows(); iRow++){

            //Remove the ID from the row custom property
            var oRow = TABLE_Object.getRow(iRow);
            oRow.propDelete(CWANZ_CONSTANTS.CWANZ_ID);

            //Iterate through the table columns and clean up the cells
            for(var c=1; c<=TABLE_Object.nColumns(); c++){

                //get a column
                var oColumn = TABLE_Object.getColumn(c);

                //get the cell that we are going to clear
                var sTargetCell = TABLE_Object.getLabel() + "." + CWANZ_numToColumn(c) + iRow;
                var oCell = document.cell(sTargetCell);
                oCell.overridden = 0;       
            }
        }

        //refresh the document
        document.recalculate(0);
    }

    /* CONSTRUCTOR */
    init();
 };

  /**
  * AUDIT 20
  * CWANZ_CRPT Object will access the Control objects and will assist in providing the data
  * required by CWANZ_TABLECONTROL to populate the table
  * @Version 1.0
  * 
  * Change Log
  * 1.0     30 07 2018      Initial version
  */
 var CWANZ_CRPT = function(){

    /* INTERNAL VARIABLE */
    var CRPT_ID_List;           //CRPT Array of IDs
    var CRPT_Data;              //The current CRPT Object
    var CRPT_Index;             //The position of CRPT Array

    /* INTERNAL FUNCTION */
    function init(){
        try{
            //Initialise control object
            var temp_CRPT = new cwWPGControl();  

            //Populate the ID Array
            CRPT_ID_List = temp_CRPT.GetControlList();

            //Reset the index
            CRPT_Index = 0;
        }
        catch(err){
            CWANZ_LogError("Unable to initialise CRPT Object" + e.message);
        }
    }

    /* EXTERNAL FUNCTION */
    this.getCount = function(){
        return CRPT_ID_List.length;
    };

    this.moveFirst = function(){
        CRPT_Index = 0;
        CRPT_Data =  new cwWPGControl(CRPT_ID_List[CRPT_Index]);
    }

    this.moveNext = function(){
        CRPT_Index++;
        CRPT_Data =  new cwWPGControl(CRPT_ID_List[CRPT_Index]);
    }

    this.atEnd = function(){
        return CRPT_Index >= CRPT_ID_List.length;
    }

    this.getValue = function(sField){
        
        //Is it the GUID?
        if(sField == "C_GUID"){
            return CRPT_Data.GetID();
        }

        //Query other data
        return CRPT_Data.GetControlFieldByID(CRPT_ID_List[CRPT_Index],sField);
    }

    this.getGUID = function(){
        return CRPT_Data.GetID();
    }

    /* CONSTRUCTOR */
    init();
 };

 /**
  * AUDIT 20
  * CWANZ_MRPT Object will access the Reportable Items objects and will assist in providing the data
  * required by CWANZ_TABLECONTROL to populate the table
  * @Version 1.0
  * 
  * Change Log
  * 1.0     30 07 2018      Initial version
  */
 var CWANZ_MRPT = function(){

    /* INTERNAL VARIABLE */
    var MRPT_ID_List;           //CRPT Array of IDs
    var MRPT_Data;              //The current CRPT Object
    var MRPT_Index;             //The position of CRPT Array

    /* INTERNAL FUNCTION */
    function init(){
        try{
            //Initialise control object
            var temp_MRPT = new cwReportableItem();  

            //Populate the ID Array
            MRPT_ID_List = temp_MRPT.GetCurrentReportableItemList();

            //Reset the index
            MRPT_Index = 0;
        }
        catch(err){
            CWANZ_LogError("Unable to initialise MRPT Object" + e.message);
        }
    }

    /* EXTERNAL FUNCTION */
    this.getCount = function(){
        return MRPT_ID_List.length;
    };

    this.moveFirst = function(){
        MRPT_Index = 0;
        MRPT_Data =  new cwReportableItem(MRPT_ID_List[MRPT_Index]);
    }

    this.moveNext = function(){
        MRPT_Index++;
        MRPT_Data =  new cwReportableItem(MRPT_ID_List[MRPT_Index]);
    }

    this.atEnd = function(){
        return MRPT_Index >= MRPT_ID_List.length;
    }

    this.getValue = function(sField){
        
        //Is it the GUID?
        if(sField == "M_GUID"){
            return MRPT_Data.GetID();
        }

        //Query other data
        return MRPT_Data.GetReportableItemProp(sField, MRPT_ID_List[MRPT_Index]);
    }

    this.getGUID = function(){
        return MRPT_Data.GetID();
    }

    /* CONSTRUCTOR */
    init();
 };


/**
 * This new script is created to re-design the way we do our mini table to ensure
 * we can document and maintain this easily
 * This mini table script will be used as the main controllers to iterate through the tables
 * and populate the table with data
 * @version 1.0
 * 
 * Change Log
 * 1.0      30 07 2018      Initial version
 */

 /**
  * AUDIT 20
  * function CWANZ_IterateTables() will go through the tables in the document
  * If it finds a table that has the custom property of CWANZ_TABLETYPE it will initialise
  * a table object and use that object to populate the table
  */
 function CWANZ_IterateTables20(){

    //Go through the tables
    for(var t=1 ; t<= document.tableCount ; t++){
        var oTable = document.tableByIndex(t);

        //Check if the table exists and has the property
        if(oTable && oTable.propExists(CWANZ_CONSTANTS.CWANZ_TABLETYPE)){
            var oTableControl = new CWANZ_TABLECONTROL(oTable);
            oTableControl.populateTable();
        }
    }
 }

 /**
  * AUDIT 20
  * function CWANZ_IterateTables() will go through the tables in the document
  * If it finds a table that has the custom property of CWANZ_TABLETYPE it will initialise
  * a table object and use that object to populate the table
  */
 function CWANZ_RefreshTable20(){
    CWANZ_alert("YEP");
 }



 function CWANZ_LogError(sMsg){
     document.messageBox("ERROR", sMsg, MESSAGE_OK);
 }
 CWANZ_IterateTables();
