/**
 * This script is created to address https://bugs.caseware.com/browse/DIST-1747
 * 
 */

clear();

//Access the CaseWare Documents

var oAccounts = document.cwClient.Accounts;

alert("Count: " + oAccounts.Count);

//Get the netINC
var oNETINC = oAccounts.get("NETINC");
if(oNETINC){
    alert("Net Inc Exist");

    //Change the mapping
    oNETINC.Groupings.Mapping = "1.1.1";
}
document.recalculate(1);



function alert(sText){
    document.messageBox("Alert", sText, MESSAGE_OK);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}