//Declare CaseWare ActiveX Object
var progressBar 	= document.createProgressBar("Initialising",1,1,1,"Initialising");

var oCaseWare = addActiveXObj("CaseWare.Application");

//Load the files
progressBar.setMessage("Loading old template");
var oOldWP = oCaseWare.Clients.Open(document.cell("OLDFILE").value);
if(!oOldWP){
    alert("Error: Failed to access the old template");
}

progressBar.setMessage("Loading new template");
var oNewWP = oCaseWare.Clients.Open(document.cell("NEWFILE").value);
if(!oNewWP){
    alert("Error: Failed to access the new template");
}

//start the test
doTest1();
doTest2();
doTest3();



function doTest1(){
    //Protection
    if(!document.cell("TEST1").value)
        return;

    progressBar.setTitle("TEST 1");

    //start the test
    print("Test 1. Mapping number does not exist in new template");
    compareMapping(oOldWP.Mappings, oNewWP.Mappings);    
}

function doTest2(){
    //Protection
    if(!document.cell("TEST2").value)
        return;

    progressBar.setTitle("TEST 2");

    //start the test
    print("Test 2. Mapping number does not exist in old template");
    compareMapping(oNewWP.Mappings,oOldWP.Mappings);    
}

function doTest3(){
    //Protection
    if(!document.cell("TEST3").value)
        return;

    progressBar.setTitle("TEST 3");

    //start the test
    print("Test 3. Comparing mapping details");
    compareMapping(oOldWP.Mappings, oNewWP.Mappings);    
}

function compareMapping(oOldMapping, oNewMapping){
    for(var m = new Enumerator(oOldMapping); !m.atEnd(); m.moveNext()){
        var oMap = m.item();

        progressBar.setMessage(oMap.Id);

        //check if this mapping exist in the new table
        if(!oNewMapping.Index(oMap.Id)){
            print(oMap.Id + " : " + oMap.Name);
        }

    }
}

function compareMappingDetails(oOldMapping, oNewMapping){
    var sSpacer = "\t";

    for(var m = new Enumerator(oOldMapping); !m.atEnd(); m.moveNext()){
        var oOldMap = m.item();

        progressBar.setMessage(oMap.Id);

        var aProblems = [];

        //check if this mapping exist in the new table
        var oNewMap = oNewMapping.Get(oMap.Id);

        //Check the details
        if(oNewMap){
            //Check Description
            if(oOldMap.Name !== oNewMap.Name){
                aProblems.push("Description: " + oOldMap.Name + " >> " + oNewMap.Name);
            }

            //Check Extended Description
            if(oOldMap.ExDescription  !== oNewMap.ExDescription ){
                aProblems.push("Extended Description: " + oOldMap.ExDescription  + " >> " + oNewMap.ExDescription );
            }

            //Check Class
            if(oOldMap.ClassType   !== oNewMap.ClassType  ){
                aProblems.push("Class: " + oOldMap.ClassType   + " >> " + oNewMap.ClassType  );
            }

            //Check Flip
            if(oOldMap.MappingFlip    !== oNewMap.MappingFlip   ){
                aProblems.push("Mapping Flip: " + oOldMap.MappingFlip    + " >> " + oNewMap.MappingFlip   );
            }

            //Check Sign
            if(oOldMap.SignType     !== oNewMap.SignType    ){
                aProblems.push("Sign: " + oOldMap.SignType     + " >> " + oNewMap.SignType    );
            }

            //Check Groupings
            for(var g=1; g<11; g++){
                if(oOldMap.Grouping(g)     !== oNewMap.Grouping(g)     ){
                    aProblems.push("Group "+ g +": " + oOldMap.Grouping(g)       + " >> " + oNewMap.Grouping(g)      );
                }

                if(oOldMap.GroupingFlip(g)     !== oNewMap.GroupingFlip(g)     ){
                    aProblems.push("Group "+ g +" Flip: "+ oOldMap.GroupingFlip(g)       + " >> " + oNewMap.GroupingFlip(g)      );
                }   
            }

            //Print the report if it exists
            if(aProblems.length){
                print (oOldMap.Id + " : " + oOldMap.Name);
            }

            for( var r=0; r<aProblems.length; r++){
                print(aProblems[r]);
            }            
        }
    }
}

function alert(sText){
    document.messageBox("Alert", sText, MESSAGE_OK);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}

