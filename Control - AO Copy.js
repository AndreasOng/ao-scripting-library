/**
 * Control Script testing
 * https://bugs.caseware.com/browse/AS-1249
 */
clear();

 //Access the control object
 var WPGControl = new cwWPGControl();

 //get all control ID
 //this function will return an array
 //["RI668350", "RI755536"]
 var aControlID = WPGControl.GetControlList();

 //Iterate through the control IDs
 for(var i=0; i<aControlID.length; i++){

    print("Control ID: " + aControlID[i]);

    //Check if the control exists
    if(WPGControl.IsControlExist(aControlID[i])){

        //Access the control object
        var objControl = new cwWPGControl(aControlID[i]);
        if(objControl){

            //Access the control id
            print("Loading control complete. ID = " + objControl.GetID());

            //Define the properties that we know
            var aControlProperties = [
                "C_NAME",       // Get control name
                "C_DESC",       // Get control description
                "C_MATURLVL",   // Maturity Level
                "C_INDIRECT",   // Indirect Control
                "C_MANUALAUTO", // Manual / Automated
                "C_FREQUENCY",  // Frequency of Control
                "C_KEYCONTROL", // Key Control
                "C_CTRLDESIGN", // Design
                "C_CTRLIMP",    //  Implementation
                "C_WALKTHRRES", // Walkthrough results
                "C_CHSINCEEV",  // Changed since previous evaluation
                "C_TESTCTRL",   // Test Control
                "C_EFFECTIVE"   // Control is effective
            ];

            //Iterate and print the properties
            for(var p=0; p<aControlProperties.length ; p++){
                print(aControlProperties[p] + " = " + objControl.GetControlFieldByID(aControlID[i], aControlProperties[p]));
            }

        }

    }
 }

 function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}
