var FINANCIALS_TAG = "Financials";

//Access the mapping Object
var oMapping = document.cwClient.Mappings;

var oProgressBar = document.createProgressBar("Updating Tags",oMapping.Count,1,0,"Updating Tags");

//iterate through the mapping
for(var m = new Enumerator(oMapping); !m.atEnd(); m.moveNext()){
    var oMap = m.item();

    oProgressBar.updateProgress(1);
    if(oMap.Id){
        oProgressBar.setMessage(oMap.Id);

        //Process the tag
        var oTags = oMap.Tags;
        if(!oTags.Get(FINANCIALS_TAG)){
            oTags.CommitMode = true;
            oTags.Add(FINANCIALS_TAG);
            oTags.Commit();
            oTags.CommitMode = false;
        }
    }
}

alert("Operation Compeleted\nYou will need to close and reopen the template to refresh the Tags");

function alert(sText){
    document.messageBox("Alert", sText, MESSAGE_OK);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}