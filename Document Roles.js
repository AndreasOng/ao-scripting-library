/**
 * This script is created to address https://bugs.caseware.com/browse/DIST-1747
 * 
 */

clear();

//Access the CaseWare Documents

var oDocuments = document.cwClient.Documents;

//Iterate through the documents
for(var d = new Enumerator(oDocuments); !d.atEnd() ; d.moveNext()){

    var oDocument = d.item();
    if(oDocument){

        //print the role settings
        print(oDocument.Id + " , " + oDocument.Name + " , " + oDocument.RoleSet );
    }
}

document.recalculate(1);



function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}