/**
 * This script will read the text file and remove the new line characters that causes an issue with AI import
 * From ::
 * 17/04/2020,Payable Invoice,"Sumanta Ghosh MasterCard - VentraIp
 * Domain Renewal - SSLCertificate 
 * 30.03.20",April MasterCard 2020 #1,218.14,0,20818.2,239.95,21.81,61150
 * To::
 * 17/04/2020,Payable Invoice,"Sumanta Ghosh MasterCard - VentraIp Domain Renewal - SSLCertificate 30.03.20",April MasterCard 2020 #1,218.14,0,20818.2,239.95,21.81,61150
 */
let fs = require('fs');
let sNewData = '';
fs.readFile('GPExV2.txt','utf8',function (err,data){
    
    let aData = data.split('\n');

    //go through each line, checking the first line
    for(let i=0; i<aData.length; i++){

        let cleanrow = aData[i].replace(/\n/gi, '');

        if(cleanrow.match(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/) ){
            if(sNewData == ''){
                sNewData = cleanrow;
            }
            else{
                sNewData += '\n' + cleanrow;
            }
        }
        else{
            sNewData += cleanrow;
        }
    }
    
    fs.writeFile('newData.txt',sNewData, (err) => {
        if(err) console.log(err);
        console.log('New data created');
    })   
})

