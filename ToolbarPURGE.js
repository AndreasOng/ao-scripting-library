/**
** Template Toolbar Script for Purge Button
**/

/**
** Disable the button if profile is equal to '3' and if not template
**/
function IsVisible()
{
	
	//Check for Micro Audit Profiles
	var oMapping = Mappings.Get("B");
	
	var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","");
	
	if((iProfile && iProfile == 3) || !oMapping)
	{

		return false;
	}
	else
	{
		return true;
	}
}

function OnClick()
{
		//Prepare the error message
	var MSG = { error:[ "OnCopyMapping()"]};
	var aMapping = ["B","C","D","E","F","G","H","I","J","M","N","O","P","Q","U","V","W","X"]; //Need to expand

	try{
		//Query the file's template ID
		var sTemplateID = ClientVersionInfo.SourceTemplateID;	//Template name
		var WPGTEMPDIR = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGTEMPDIR"); //Template directory
		var WPGVER = CaseViewData.DataGroupFormId("WPG_TEM_DATA","AUDITSYSTEM","WPGVER"); //Template version
		
		//Protection Level 1. Protection Check if there are values in the versions
		if(sTemplateID && WPGTEMPDIR && WPGVER)
		{
			//Protection Level 2. Protection, check the versions
			//This to make sure we have the right scripts for the right versions
			//messageBox(sTemplateID,WPGTEMPDIR + "\n" + WPGVER,MESSAGE_OK);
			if(sTemplateID == "Audit System" && WPGTEMPDIR == "Audit System")
			{
				//Protection Level 3. Protection based on the Profile
				//Get the profile index
				// 1. Australia
				// 2. New Zealand
				// 3. SMSF ---- DISABLED
				// 4. Review
				// 5. One Form Australia
				// 6. OneForm New Zealand
				// to beupdated
				var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","");
					
				if(iProfile && iProfile !== 3){
					messageBox("Alert", "We are Purging", MESSAGE_OK);

					if(mappingExist(aMapping)){
						//Do we have purge function?
						var oPurge = Purges.Get("MICRO (DEL)");
						if(oPurge){
							oPurge.PurgeNow();
						}
						else{
							//Run manual purge
							mappingPurge(aMapping);
						}
					}
				}
			}
		}
	}
	catch(err){
		MSG.error.push(err.message);	
	}
	
	CaseViewData.DataGroupFormId("CWANZ","LOG","OnCopyMapping") = JSON.stringify(MSG);

	function mappingExist(aMappingList){
		for(var i=0; i<= aMappingList.length; i++){
			if(Mappings.Get(aMappingList[i])){
				return true;
			}
		}
		return false;
	}

	function mappingPurge(aMappingList){
		for(var i=0; i<= aMappingList.length; i++){
			if(Mappings.Get(aMappingList[i])){
				Mappings.Remove(aMappingList[i]);
			}
		}
	}
}
