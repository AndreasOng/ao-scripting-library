/**
 * This OnFileOpen will be executed on Audit System template only
 * The goal is to check for Folder ID and whether it is corrupted
 *
 * Thinking:
 * We need to alert all the users who opens this engagement file about the issue. 
 * This way we can ensure that all the users has been notified until the issue is addressed
 * This needs to be sync safe, so we need to ensure that we don't generate sync conflicts too.
 * 
 * @author CaseWare Australia and New Zealand
 * @method OnFileOpen()
 * @version 1.0 - 20 11 2020
 */
function OnFileOpen() 
{
	//Standard messages to the user
	var sAlertMessage = "Folder duplication";
	
	//Query the file's template ID
	var sTemplateID = ClientVersionInfo.SourceTemplateID;
	var sSecondaryTemplateID = CaseViewData.DataGroupFormId("","","WPGTEMPDIR_AT_BUILD");
	
	//get the user details
	var sUserID = WhoAmI();
	var oUserDetails = Security.Users.Get(sUserID);
	var sUserName = oUserDetails.FullName;
	
	//get the date time stamp
	var dNow = new Date();
	var sDate = dNow.toString();
	
	//Filter if the file is OK
	if(CaseViewData.DataGroupFormId("CWANZ","FLAG","OnFileOpen18617"))
		return;
	
	//Filter to check if we have warned the users
	if(CaseViewData.DataGroupFormId("CWANZ","FLAG18617",sUserID))
		return;

	//Check if file is not locked down
	if(IsLockedDown)
		return;
	
	//Make sure that this is not the template
	if(!sTemplateID || FilePath.indexOf("\\Template")>=0 || FilePath.indexOf("\\Document Library")>=0)
		return;
		
	//Audit System  || Public Sector
	if(sTemplateID != "Audit System" && sTemplateID != "Public Sector" && sSecondaryTemplateID != "AUDITSYSTEM" && sSecondaryTemplateID != "PUBLICSECTOR" )
		return;
	
	
	/**
	 * This is the reference IDs
	 */
	//Audit System Document ID - From Build 65
	var auditSystemID = 
		{"Engagement File Setup":"OFZ4+2oR0KxJQ5gvhHremi","Phase A - Risk Assessment":"G5TCqwsC0NmHvxE2jcQGe1","1 - Perform Preliminary Engagement Activities":"EGqXTI6jUgoOeMyrAgkoeP","1-100 - Independence":"Ms2BAXe+kNWKKRoflJgb69","1-200 - Pre-Engagement Activities":"I0OjnPs50EXGISivPI6QGS","1-300 - Terms of Engagement":"OFbkoHIjk5XLx4RF4o44Wj","2 - Plan the Audit":"FuCT2BSN0rBKLSHofeww6g","2-100 - Understanding the Entity":"HARhUV2y0gBA5UKJ2Gw5WX","2-200 - Materiality":"EfMkWHg3kaTKf-aLlyfi6s","2-250 - AFSL Materiality Considerations":"DOT52iaOkXRJo4I1hHztm9","ASAE 3000 - AFSL Materiality Considerations":"I0CJx0akkVrPjh-H9vGoWl","ASAE 3100 - AFSL Materiality Considerations":"KKIdMPmrUCbPVtzy2BoNK-","ASAE 3150 - AFSL Materiality Considerations":"CePIxJkokrrPIVfqBEnjeI","ASAE 3450 - AFSL Materiality Considerations":"JSFQbPKeka2LhOFyu3XKuB","2-300 - Other Considerations":"E5JxncN2ElqK+fBUR5DCCu","2-400 - Audit Strategy":"Ktl5JBD403EJQPYWhZBgSf","3 - Perform Risk Assessment Procedures":"GtH1QMh30yfEpADTYhAdWU","3-100 - Preliminary Analytical Procedures":"CHujjRgF0C3BCnUlIra+eY","Consolidation":"Dz2XVD1uEl-Jdj7xRq8fWr","3-150 - Design and Implementation of Controls":"KZzjLlbo0+2GPayHDXaiKi","Revenue / Receivables / Receipts (RRR)":"IGzrVdCE0hNMahUbUbJoOf","Expenses / Payables / Payments / Payroll (EPP)":"LtzY5vcPUq6OwzCn+zAXS8","Estimates, Assumptions & Judgements (EAJ)":"LR7m8amRUSGLqe7Phvlm2E","Financial Reporting & Disclosures (FRD)":"CF3qRQc2047INe+USE95ud","3-200 - Concluding on Risk Assessment Phase":"EpJVujtF0PXFtErRz5QFSd","3-300 - Interim Management Letter & Reporting to Those Charged with Governance":"JWC0Hzdpkl6Ol-hALQ8gGm","Phase B - Risk Response":"EyPtWSyiEctEGJf2zyx6qf","A - General":"CIVqyMq-01EICUY5NT-O6U","B - Cash And Cash Equivalents":"AqIfnGlHkmsAyRh9oGGRSp","C - Trade And Other Receivables":"G8NryGSzENeCzof2rcQyeg","D - Financial Assets":"B54g-xfkEe6KaM5TQ8r+qm","E - Inventories":"GADpKF+kE4uLMStpRoY2SJ","F - Assets Held For Sale":"E4we4jn2UMtFBp+K0Sc2+B","G - Property, Plant & Equipment":"LhWx1TLJk8YLFFPmNKy32P","H - Intangible Assets":"LLvpzwWCkwxGXX7KvzKtSX","I - Investments":"OX4yTBQA0tmGaHq4zbZsuE","J - Other Assets":"JviiY9ThkSgHnSrdJLSb2N","L - Right-of-use assets":"PUpX9gI+UlEJT6wE-R-pWH","M - Tax Balances":"FL1uIZN9U8KCWaRuTPkt6o","N - Trade And Other Payables":"K2DdnwG80FuCWGwzQg2ZWp","O - Financial Liabilities":"Ps1R-Pv60z9NGyxsZ04Imn","P - Provisions":"C-+ORRhuEq0NkgUrB7xVaP","Q - Other Liabilities":"LWLHNiUZETDGG0akut6hmf","R - Contingencies & Commitments":"HHa3D689kOJJWi4tZdHaSG","S - Lease Liabilities":"DwtsnU5oEvWKTyn0nijmyd","U - Equity":"MB4idHAYEhTFlTOK4RYaSR","V - Revenue":"ORsaNdsrE1lIZmuuoVt5mE","W - Expenses":"FEGRoHU40ETNNTXuPiXl64","X - Payroll":"NLfwm3PQ0g9EzvLouiKyOQ","Z - Financial Statement Areas and Balances":"AkA466m00kqDlli3tPZgGt","Assets":"FtWo4ay4EpVCAzOy4VS0+l","Liabilities":"H084CscaU54IPYy431LRO+","Revenue and Expenses":"F+WhtIusExMPus0GANo-GV","Other Financial Areas":"E368dNtMkfuBLNvfth6WSQ","Phase C - Reporting":"BfDOktplEz-PZI59pqbaep","4-100 - Journals":"KXWZQw9HEgtMDsZ9vG1cS4","4-200 - Subsequent Events and Going Concern":"PF6h85RjEtAJGrbT3pOmOk","4-300 - Financial Statements":"KiFA8wPwEKDJf0Z91nibah","4-400 - Related Parties, Subsequent Events and Going Concern":"DMCyljscUMIOVRahbb1+y3","4-700 - Audit Opinion":"PzXI-Xlkk+IL2vZXFl5QSA","4-800 - Conclusion on Fraud and Management Bias":"MfXOUUctk2mF2oFoT4xdW+","5-100 - Management Representations":"EBhvzQkK0AuLlBLnFz2rSR","5-200 - Management Letter & Reporting to Those Charged with Governance":"Lz9AV5MJ0hsJBCsXH7Xn+k","5-300 - Quality Control":"Dk8tNz1g0pNJ-XUn7VNLiF","6-100 - Completion":"Np8pxPPwkoICzCDqeyzCWl","Outstanding Items":"NAFFe-lg0jAKezdlGW5le3","Points for Partner Attention":"HeMHEU+p0SbDh5rveUl5eO","Review Points & Points forward":"BSqNJbUK06fDZYl0aGAvmm","History":"OK89qt3FkFYHvw+TCRQ+Sk","Audit System - Control Documents (do not delete)":"CawBx8LSE65JfYHLbvhDOo","Admin":"BUekkKbXE0YIwsP1muZAqj","Engagement File Updates":"DSFCUQX80FdEAmxnsenp6z","Cloud Documents":"OgynQuP-EGiMjq5hz33F6N","Master Documents":"H7jBDay10zbAXK873y8dSF","Update":"MjMRcwSKkL7LMkxF8do8Ga","Firm Documents":"EA42nIHfUOSLa4DMXtycad","XTEND ANZ Documents":"OP0mn1fKUaIFYoUa7a3HC5"};
	
	//PSAS Document ID -- Build 78  
	var publicSectorID = 
		{"Engagement file setup":"OUopv8oMk1iCGOFcQzoMeE","Permanent Information":"Gpo2L4woER+JuTqdWh8rK1","Client Engagement":"Ad403nVs0fMEl-y5lw40Ku","Background Information":"Mtg1emlUUccExwpHTTo92r","Contracts and Agreements":"GwKTmA4UUhYH8e7Q11kRyw","Other Information":"JvXzUE2BEHsPiaS6NeJjSA","Phase A - Risk Assessment":"LXwYfeLoULvEtpbaJKAgaK","1 - Preliminary Engagement Activities":"GSYiDYYm05LEYF8vJsF2u8","1-100 - Independence":"OyEPk6bykb+L7OvgdoFJqT","1-200 - Pre-Engagement Activities":"AyA7WV2l05OJi7s9aHF0+l","1-400 - Trial Balance":"F0vffx07kNZF8B48a5i1W4","1-450 - Consolidated Trial Balance":"H6vjNAcq0bNJhJZx5ioUS-","2 - Entity Level Planning":"PXyeCb08UBpGKxLXyI2xSQ","2-100 - Understanding the Entity":"E3OAbJfXkI-HcowtBut3q+","2-150 - Understanding the IT Environment (Including GITC)":"IhdKekJDET-ChhWzK-uLK5","2-200 - ASA Considerations":"JXxijSXc0AYF1pcj0YU+GI","Fraud (ASA 240)":"IC6C8MQHEL+KjbG+vjUQ+m","Laws and Regulations (ASA 250)":"ISE7m3c1UHBHvlko+4ejSR","Estimates (ASA 540)":"BW4ByA0SECtC-9JTaKCcmx","Related Parties (ASA 550)":"OxqEeIJgUy0FbT9RT957yZ","Group Audit (ASA 600)":"FBzMYKSh0EuKJGWh3g9tmr","Internal Audit (ASA 610)":"Js82j-cdkIuP70R7simRuw","Other ASA Considerations":"HNX0kvHBEaSHcU6oxiyTqa","2-300 - Risk Assessment Procedures":"OA6MH46tEFFMGRiNG84KuZ","2-400 - Audit Strategy, Plan and Materiality":"FahMmIfGEWfMFdniue6aey","2-500 - Communication":"AktVumCnU-zAx29rdfv42B","The Client":"IT82F1jyUsxBR86WCTw+C9","Audit Service Provider (ASP)":"JQcoMcTEkppG-iM8lgL0iS","3 - Assertion Level Planning":"MTsEmk5tkKnAoBsFdtniqL","3-100 - Public Sector Specific Audit Assertions":"LMzI4xIsU8PLD-5Uj7SwiS","3-200 - Design and Implementation of Controls":"NZbciFhO0fpBs3J0GX+Gik","Revenue":"GpO2BHQIUlcG50cgFIkj+-","Expenses":"FLe8Lf6Uk+hKNMavkysJyk","Payroll":"EASqu1yT05jAVQwsCc3Kem","Property, Plant and Equipment":"GWScq+OY0aIPdBK42tbDCH","Other Areas (if applicable)":"MnGYwOwmketJIliQd5niCD","Cash & Financing":"MU5ENE6e0p9ClX36k+I92z","Inventory":"CvXwTFUnUpRJN9PlM6WGOW","Equity":"DgD+kM0P0IzBzHvrbalUup","3-500 - Conclude on Risk Assessment":"ByeSyPP0EaqLvYMNxV+WKl","Phase B - Risk response":"Gr3Xt5bykGyIkfRcN-7m+9","A - Journals":"J0+-OfvS0r+P-0Yz+JNiyI","B - Cash and Cash Equivalents":"IHXFeWzvEt+PZSVjlR26O9","Leadsheets":"D7iOPb9YU4dCnkaLoC1DOr","Risk response programs":"NrSm8K3gkWAPClVwpI6ieY","Tests of control":"D8d6fkDzUUJOkLjm-iB7us","Substantive analytical procedures":"NOUZvkA1kNoGuavjjAeiSY","Tests of detail":"PmQRcJYFky5LFwygp3tWyp","C - Trade and Other Receivables":"PDv4rYN10IOHC-IoYPWbWI","D - Financial Assets":"IlTkovHd084HwX4v-XIRCv","E - Inventories":"DVJYthu3UarAwsC+KotFiQ","F - Assets Held For Sale":"DuVxhHho0grOLUx1WRphqj","G - Property, Plant & Equipment":"KbMjI7x8Ei8A8984f79jGW","H - Intangible Assets":"OY6lWxoCk9yMFcmAF0DHu-","I - Investments":"CthOwsoSU6aHT7S+UKkcyq","J - Other Assets":"FNjRPDAlk6jC3A+5W-3aCZ","L - Right-of-use assets":"L7J3f6V5UvMGaphCabnT2J","M - Tax":"HFUUO3T6EjPLVXmMZQR8S-","N - Trade and Other Payables":"NH5ifJIa06gA8jRE2yrr6F","O - Financial Liabilities":"P9FDwt6oEmJMMCjFTowwWe","P - Provisions":"F8MyM-950LnOljb7MgsBex","Q - Other Liabilities":"LgXqhxLmEb9Jm5FbUJ1e2r","S - Lease Liabilities":"IY8vSCbN0qNAtEaLJPaGCs","U - Equity":"EBkHPK9u0nhIqN5eXdh9en","V - Revenue":"JDfgh17QE+6PIS03v4uGuu","Leadsheets - Local Government":"F8hHtUyCkiGBSEPSa9RQmI","W - Expenses":"LXBCFJETkjHL5uIRbCuBKq","X - Payroll":"PnGy33nd0prN2CJzcmusun","Z - Other Audit Areas":"JoEuQrFeEO6OS3y2z6fAG7","Z1 - Contingencies and Commitments":"NGBkNDT2kQNLIKp5dvKOSc","Z2 - Related Parties":"EC8Hxdvy0CQAK4gsAx7mew","Z3 - Subsequent Events":"GK1+WNq1E8MNuXiAjJHoaZ","Z4 - Going Concern":"PXIhiYjZkPmB38ymDoDPm-","Z5 - Administered Items":"IyR+6b050LSDo7FY+g6tmo","Z6 - Public Private Partnership Arrangements":"LRK1zbmZ03SCU5ejsfdpCU","Z7 - Budgetary Reporting":"EdAHyZxfEmGGxc-SV8PGGM","Z8 - Business Combinations":"BVCc1uT-E8dOEpw0LitgK0","Phase C - Reporting":"MYz+41O+kH-J77SwG5wSmt","4 - Financial Statements, Journals & Opinion":"CLHRihPKU8YLvfoVP5B0Ka","4-050 - Accounting Position Papers":"K6HQ206MUOgORWcv7N0Y+L","4-100 - Financial Statements":"Fbd99CeF0R-DS1Y8uJzfSW","4-200 - Management Representations":"DqmUYIdhU8QMafWKQUNO6N","4-300 - Misstatements (Corrected and Uncorrected)":"KIS0x29-kiJBMRIrZbxRSZ","4-700 - Audit Report":"FIPy8mXvE+TFTtXRnBQgun","Prescribed Requirements (if applicable)":"Gx64NZHEUG9KEgO46aMuyp","5 - Other Related Engagements":"Hipl4uHkUE8OI2eoDHB-CB","5-100 - Grant Aquittals":"Ba3nRylH0pOF3D3ayBjciw","5-200 - Performance Reporting":"GijZYHzTkryLdE-EegbEa0","5-300 - Whole of Government (WOG) Reporting":"G1-aNzJEkYfEAO3TZkMV+S","Testing Work Papers":"GQ2zNVjbU0QOTNyRWZ2eCb","6 - Communication":"Kn-7ZIzbEQbOtCQB+Qfd2-","6-100 - The Client":"CaB7Ts7+kqaHxIQSimQPSd","6-200 - The Audit Team":"K6dG+qYg0bqAGZjSawIU65","7 - Quality Control":"GX26XX3RUEhINwb1cHcxCy","7-100 - ASP engagement Leader Review":"B6InmOyKkmdA7MljMAufeV","7-200 - Engagement Leader, EQCR and Signing Officer":"BSjGoEebU3FKqOh+7-nBqS","ASA Compliance Document":"FVO14Ds3Uh4J24ZolPs-ub","8 - Completion":"AB88IlHnkiPJBjaYABstyu","Outstanding Items":"AVVOgmVk0TGPPdKubmFHqS","Points for EL Attention":"FhFMD0KoUI2NjSE6Vhggy+","Review Points & Points forward":"ONCbwz4nEFNIQezcHBOsOc","History":"Ldlf+3oxEK1DGsHpzyTR6m","File Finalisation, Administration and Rollover":"A-dUVstaEEnCDYT0ohSnCY","PSAS - Control Documents (do not delete)":"AMQ5b9RQ0V6B9B7+JGTtGE","Admin":"Do9Y+wKH0MaO0dU5OEnYKV","Cloud":"Mrqpiqt+U87OmvI939TnOc","Master":"G0zDInYPk62FaMrrT7w2Cw","Update":"ARoY-lKaUlhP-Zw7UO7NiP","Settings":"EQ2truDk0E3DTf3iMFCQiR","XTEND ANZ Documents":"Pe63cArzEawMVfyqwkR+Km"};
	
	
	/**
	 * Performs the checks
	 */
	//Prepare the error message
	var MSG = {};
	MSG["Template ID"] = sTemplateID;
	
	//Run the comparison based on template type
	var aCheck;
	if (sTemplateID == "Audit System" || sSecondaryTemplateID == "AUDITSYSTEM" ){
		aCheck = checkCorruptID(auditSystemID);
	}
	else{
		aCheck = checkCorruptID(publicSectorID);
	}
	
	//Process the results
	if(aCheck.length > 0 ){
		
		//Alert the user
		messageBox("Alert", sAlertMessage ,1);
		MSG["DATE CHECKED"] = sDate;
		MSG["USER"] = sUserName;
		MSG["FOLDER"] = aCheck.length;
		MSG["RESULTS"] = aCheck;
	}
	else{
		MSG["DATE CHECKED"] = sDate;
		MSG["USER"] = sUserName;
		MSG["RESULTS"] = "ALL Good";
		CaseViewData.DataGroupFormId("CWANZ","FLAG","OnFileOpen18617") = 1;
	}
	
	
	log(JSON.stringify(MSG));

	
	/*****************************************************************************************************************/
	
	//Internal functions
	function checkCorruptID(oRefDoc){
		
		var aResult = [];
		
		//go through the folder and compare the ID
		for(var e = new Enumerator(documents); !e.atEnd(); e.moveNext()){
			var oDoc = e.item();
			
			//is it a folder
			if(oDoc.type == 0) { //dtFolder
				//lets check the GUI
				
				var sDocGuid = oDoc.ItemProperty("CWGUID");
				
				//Do we have it in the template
				if(oRefDoc[oDoc.Name]){
					var sTempGuid = oRefDoc[oDoc.Name];
					
					//Compare it
					if(sDocGuid != sTempGuid){
						aResult.push(oDoc.Name);
					}				
				}
			}
		}
		
		return aResult;
	}
	
	
	function log(sMsg){
		//Store messages to the CaseView Data
		CaseViewData.DataGroupFormId("CWANZ","FLAG18617",sUserID) = sUserName;
		CaseViewData.DataGroupFormId("CWANZ","LOG18617",sUserID) = sMsg;	
	}
	
}








