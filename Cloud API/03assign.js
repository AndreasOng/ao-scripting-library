//188,Local Owner
//11,ABC Audit

var request = require('request');
var settings = require('./config.json');
var entityRules = require('./entityrules.json');

console.log("Authenticating to Caseware Cloud");
login();


function login()
{
    var sUrl = settings.CloudURL + '/CWCoreService/restricted/SessionService.svc/Login';
    
    var oPayload = {
      applicationType: "Browser",
      firmName: settings.CloudFirm,
      language: "en",
      loginName: settings.UserName,
      password: settings.Password,
      rememberMe: true,
    };

   request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('Login error:', error);
            } else {
                settings.UUID = body.d.result.UUID;
                console.log("Login successful UUID: " + settings.UUID);

                console.log("Loading Entity");
                GetEntities(0);
            }
        }
    );
}

function GetEntities(iBatchPos){
     //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/EntityService.svc/GetList
     var sUrl = settings.CloudURL +'/CWCoreService/restricted/EntityService.svc/GetList';
    
     var oPayload = {
        "F":{
                "RetrieveList":[{"ObjectType":"Entity","Fields":["Confidential","EffectivePermissions","OwnerType","EntitySubtypeId","Subscriptions","IsSubscribed","HasContacts","Name","Location","EntityNo","YearEndMonth","Tags"]}],
                "BatchSize":31,
                "BatchStart":iBatchPos,
                "OmitTotal":true,
                "SortBy":[{"Field":{"FieldName":"Name","Type":"BaseModel","ExpressionType":"FilterField"},"Ascending":true}]
            },
        "Uuid":settings.UUID};


    request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                //console.log("Security Settings Received");               
                var list = body.d.result.result;

                for(var i=0; i<list.length; i++){
                    //console.log(list[i].Id + ","+  list[i].EntityNo + "," + list[i].Name);
                    checkEntity(list[i]);
                }

                //need more?
                if(list.length > 0){
                    GetEntities(iBatchPos + 30);
                }
            }
        }
    );
}

function checkEntity(oEntity){
    //Check whether we need to process this entity
    var aData = entityRules.Entity;

    for(var i=0; i<aData.length; i++)
    {
        //Get the entity no, need to trim it
        var sEntityNo = oEntity.EntityNo.replace(/\s/gi,"");
        //console.log(sEntityNo + "," + sEntityNo.substring(0,aData[i][0].length) + "," + aData[i][0]);
        
        if(sEntityNo.substring(0,aData[i][0].length) == aData[i][0]){
            setSecurity(oEntity, aData[i][1], aData[i][2]);
        }
    }
//console.log(list[i].Id + ","+  list[i].EntityNo + "," + list[i].Name);
}

function setSecurity(oEntity, iGroup, ipermission){
    //console.log(oEntity.Id + ","+  oEntity.EntityNo + "," + oEntity.Name + "," + iGroup + "," + ipermission);

        //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/RoleService.svc/AssignRemoveObjectRoles
        var sUrl = settings.CloudURL +'/CWCoreService/restricted/RoleService.svc/AssignRemoveObjectRoles';

        var oPayload = {
            "Changes":[{
                "Id":iGroup,
                "IsPerson":false,
                "IsRemove":false,
                "ObjId":oEntity.Id,
                "ObjType":"Entity",
                "RoleId":ipermission
                }],
            "Uuid":settings.UUID};


    request.post({
        url : sUrl,
        body: oPayload,
        json: true
    }, function (err, response, body) {
            if(err){
                console.log("FAIL - " + oEntity.EntityNo + "," + oEntity.Name);
            } else {
                console.log("OK  - " + oEntity.EntityNo + "," + oEntity.Name);               
            }
        }
    );
}
