var request = require('request');
var settings = require('./config.json');

console.log("Authenticating to Caseware Cloud");
login();


function login()
{
    var sUrl = settings.CloudURL + '/CWCoreService/restricted/SessionService.svc/Login';
    
    var oPayload = {
      applicationType: "Browser",
      firmName: settings.CloudFirm,
      language: "en",
      loginName: settings.UserName,
      password: settings.Password,
      rememberMe: true,
    };

   request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('Login error:', error);
            } else {
                settings.UUID = body.d.result.UUID;
                console.log("Login successful UUID: " + settings.UUID);

                console.log("Loading Security Groups");
                viewSecurity();
            }
        }
    );
}

function viewSecurity(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/RoleService.svc/GetList
     var sUrl = settings.CloudURL +'/CWCoreService/restricted/RoleService.svc/GetList';
    
     var oPayload = {
        "F":{
                "BatchSize":2147483647,
                "OmitTotal":false
            },
        "Uuid": settings.UUID
    };


    request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log("Security Settings Received");               
                var list = body.d.result.result;

                for(var i=0; i<list.length; i++){
                    //Search staff groups
                    if(!list[i].BuiltIn && list[i].ForStaff){
                        //console.log(list[i]);
                        console.log(list[i].Id + ","+list[i].Name);
                    }
                }
            }
        }
    );   
}

