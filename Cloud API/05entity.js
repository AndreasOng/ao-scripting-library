//188,Local Owner
//11,ABC Audit

var request = require('request');
var settings = require('./config.json');

var entityRules = require('./entityrules.json');

console.log("Authenticating to Caseware Cloud");
login();
//GetEntities(0);

function login()
{
    var sUrl = settings.CloudURL + '/CWCoreService/restricted/SessionService.svc/Login';
    
    var oPayload = {
      applicationType: "Browser",
      firmName: settings.CloudFirm,
      language: "en",
      loginName: settings.UserName,
      password: settings.Password,
      rememberMe: true,
    };

   request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('Login error:', error);
            } else {
                console.log(JSON.stringify(body))
                settings.UUID = body.d.result.UUID;
                console.log("Login successful UUID: " + settings.UUID);

                console.log("Loading Entity");
                GetEntities(0);
            }
        }
    );
}

function GetEntities(iBatchPos){
     //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/EntityService.svc/GetList
     var sUrl = settings.CloudURL +'/CWCoreService/restricted/EntityService.svc/GetList';
    
     var oPayload = {
        "F":{
                "RetrieveList":[{"ObjectType":"Entity","Fields":["Confidential","EffectivePermissions","OwnerType","EntitySubtypeId","Subscriptions","IsSubscribed","HasContacts","Name","Location","EntityNo","YearEndMonth","Tags"]}],
                "BatchSize":31,
                "BatchStart":iBatchPos,
                "OmitTotal":true,
                "SortBy":[{"Field":{"FieldName":"Name","Type":"BaseModel","ExpressionType":"FilterField"},"Ascending":true}]
            },
        "Uuid":settings["UUID"]};


    request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                //console.log("Security Settings Received");      
                //console.log(JSON.stringify(body));         
                var list = body.d.result.result;

                for(var i=0; i<list.length; i++){
                    //console.log(JSON.stringify(list[i]));
                    console.log(list[i].Id + ","+  list[i].EntityNo.trim() + "," + list[i].Name);
                   //return;
                }

                //need more?
                if(list.length > 0){
                    GetEntities(iBatchPos + 30);
                }
            }
        }
    );
}
