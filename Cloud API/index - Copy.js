var request = require('request');

var sUserName = 'ao@caseware.com.au';
var sPassword = '@dmin1234';
var sCloudURL = 'https://au.casewarecloud.com/test-soeadvisory';
var sCloudFirm = 'test-soeadvisory';

var UUID;

console.log("Logging into the Cloud");
login();


console.log("UUID:" + UUID);

console.log("Getting Enttiy list");
//getEntityList();

console.log("Assign Entitylist");
//assignEntity();

console.log("View Staff Groups");
GetSecurity();

function login()
{
    var sUrl = sCloudURL + '/CWCoreService/restricted/SessionService.svc/Login';
    
    var oPayload = {
      applicationType: "Browser",
      firmName: sCloudFirm,
      language: "en",
      loginName: sUserName,
      password: sPassword,
      rememberMe: true,
    };

   request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                //console.log('body:', body);
                //Collect the UUID information
                UUID = body.d.result.UUID;
                console.log(UUID);
            }
        }
    );
}

/*
* Payload info 
{"F":{"RetrieveList":[{"ObjectType":"Entity","Fields":["Confidential","EffectivePermissions","OwnerType","EntitySubtypeId","Subscriptions","IsSubscribed","HasContacts","Name","Location","EntityNo","YearEndMonth","Tags"]}],"BatchSize":31,"OmitTotal":true,"SortBy":[{"Field":{"FieldName":"Name","Type":"BaseModel","ExpressionType":"FilterField"},"Ascending":true}]},"Uuid":"09ba1f11-9f7d-9c6e-fa4c-dd71ded440f6"}
*/
function getEntityList(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/EntityService.svc/GetList
    var sUrl = sCloudURL +'/CWCoreService/restricted/EntityService.svc/GetList';
    
     var oPayload = {"F":{"RetrieveList":[{"ObjectType":"Entity","Fields":["Confidential","EffectivePermissions","OwnerType","EntitySubtypeId","Subscriptions","IsSubscribed","HasContacts","Name","Location","EntityNo","YearEndMonth","Tags"]}],"BatchSize":31,"OmitTotal":true,"SortBy":[{"Field":{"FieldName":"Name","Type":"BaseModel","ExpressionType":"FilterField"},"Ascending":true}]},"Uuid":"625b782e-ce50-2428-4b65-bafcd7864815"}
     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('body:', body);               
                var entityList = body.d.result.result;
                console.log(entityList[0]);
                //EntityNo:
                //Name:
                //Id:
            }
        }
    );   
}

function assignEntity(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/RoleService.svc/AssignRemoveObjectRoles
     var sUrl = sCloudURL +'/CWCoreService/restricted/RoleService.svc/AssignRemoveObjectRoles';
    
     var oPayload = {"Changes":[{"Id":17,"IsPerson":false,"IsRemove":false,"ObjId":999,"ObjType":"Entity","RoleId":1}],"Uuid":"625b782e-ce50-2428-4b65-bafcd7864815"};
     
     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('body:', body);               
                //var entityList = body.d.result.result;
                //console.log(entityList[0]);
                //EntityNo:
                //Name:
                //Id:
            }
        }
    );   
}

function viewSecurityGroups(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/PersonService.svc/GetSecurityGroupList
     var sUrl = sCloudURL +'/CWCoreService/restricted/PersonService.svc/GetSecurityGroupList';
    
     var oPayload = {"ShowAllPeopleGroups":true,"F":{"BatchSize":1000,"OmitTotal":true},"Uuid":"625b782e-ce50-2428-4b65-bafcd7864815"}


     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('body:', body);               
                var entityList = body.d.result.result;
                console.log(entityList[0]);
                //EntityNo:
                //Name:
                //Id:
            }
        }
    );   
}

function viewRoles(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/RoleService.svc/GetRolesForObject
     var sUrl = sCloudURL +'/CWCoreService/restricted/RoleService.svc/GetRolesForObject';
    
     var oPayload ={"ObjId":999,"ObjectType":"Entity","F":{"BatchSize":0,"OmitTotal":false,"FilterBy":{"CompositeOpType":"And","PredicateType":"Composite","LeftPredicate":{"LeftExpression":{"FieldName":"OwnerType","Type":"BaseModel","ExpressionType":"FilterField"},"CompareOpType":"eq","PredicateType":"Compare","RightExpression":{"LongValue":0,"ExpressionType":"FilterValue"}},"RightPredicate":{"CompositeOpType":"And","PredicateType":"Composite","LeftPredicate":{"PredicateType":"Not","RightPredicate":{"PredicateType":"IsNull","Expression":{"FieldName":"RoleId","Type":"BaseModel","ExpressionType":"FilterField"}}},"RightPredicate":{"LeftExpression":{"FieldName":"IsGroup","Type":"BaseModel","ExpressionType":"FilterField"},"CompareOpType":"eq","PredicateType":"Compare","RightExpression":{"BoolValue":false,"ExpressionType":"FilterValue"}}}}},"Uuid":"625b782e-ce50-2428-4b65-bafcd7864815"}


     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('body:', body);               
                var entityList = body.d.result.result;
                console.log(entityList[0]);
                //EntityNo:
                //Name:
                //Id:
            }
        }
    );   
}

function GetSecurity(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/RoleService.svc/GetList
     var sUrl = sCloudURL +'/CWCoreService/restricted/RoleService.svc/GetList';
    
     var oPayload ={"F":{"BatchSize":2147483647,"OmitTotal":false},"Uuid":"625b782e-ce50-2428-4b65-bafcd7864815"}

     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('body:', body);               
                var entityList = body.d.result.result;
                console.log(entityList[0]);
                //EntityNo:
                //Name:
                //Id:
            }
        }
    );   
}

function GetStaffGroup(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/PersonService.svc/GetSecurityGroupList
     var sUrl = sCloudURL +'/CWCoreService/restricted/RoleService.svc/GetList';
    
     var oPayload ={"F":{"BatchSize":2147483647,"OmitTotal":false},"Uuid":"625b782e-ce50-2428-4b65-bafcd7864815"}

     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('body:', body);               
                var entityList = body.d.result.result;
                console.log(entityList[0]);
                //EntityNo:
                //Name:
                //Id:
            }
        }
    );   
}

