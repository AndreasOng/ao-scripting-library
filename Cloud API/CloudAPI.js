//188,Local Owner
//11,ABC Audit

var request = require('request');
var settings = require('./config.json');

//Login Promise Script
let login = new Promise((resolve, reject) =>
{
    var sUrl =
    settings.CloudURL + "/CWCoreService/restricted/SessionService.svc/Login";

    var oPayload = {
        applicationType: "Browser",
        firmName: settings.CloudFirm,
        language: "en",
        loginName: settings.UserName,
        password: settings.Password,
        rememberMe: true,
        RememberTwoFactorAuthentication:false,
        TwoFactorAuthenticationClientSecret: settings.TFASecret
    };

    request.post(
        {
            url: sUrl,
            body: oPayload,
            json: true,
        },
        function (err, response, body) {
        if (err) {
            console.log("Login error:", err);
            reject(err)
        } else {
            settings.UUID = body.d.result.UUID;
            console.log("Login successful UUID: " + settings.UUID);
            resolve(settings.UUID)
        }
    }
    );
});

let listEngagement = function(UUID){
    return new Promise((resolve,reject) => {
        console.log(UUID);
    });
}
login.then(listEngagement);