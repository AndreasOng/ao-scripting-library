const inventory = {
    sunglasses: 1900,
    pants: 1088,
    bags: 1344
  };
  
  // Write your code below:
  function myExecutor(resolve, reject){
    if(inventory.sunglasses >0){
      resolve('Sunglasses order processed.')
    }
    else{
      reject('That item is sold out.')
    }
  }
  
  function orderSunglasses(){
    return new Promise(myExecutor)
  }
  
  let orderPromise = orderSunglasses()
  console.log(orderPromise)



  let prom = new Promise((resolve, reject) => {
    let num = Math.random();
    if (num < .5 ){
      resolve('Yay!');
    } else {
      reject('Ohhh noooo!');
    }
  });
   
  const handleSuccess = (resolvedValue) => {
    console.log(resolvedValue);
  };
   
  const handleFailure = (rejectionReason) => {
    console.log(rejectionReason);
  };
   
  prom.then(handleSuccess, handleFailure);



  const {checkInventory} = require('./library.js');

const order = [['sunglasses', 1], ['bags', 2]];

// Write your code below:
function handleSuccess(resolveValue){
  console.log(resolveValue)
}

function handleFailure(rejectValue){
  console.log(rejectValue)
}

checkInventory(order).then(handleSuccess, handleFailure)



const inventory = {
    sunglasses: 1900,
    pants: 1088,
    bags: 1344
  };
  
  const checkInventory = (order) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let inStock = order.every(item => inventory[item[0]] >= item[1]);
        if (inStock) {
          resolve(`Thank you. Your order was successful.`);
        } else {
          reject(`We're sorry. Your order could not be completed because some items are sold out.`);
        }
      }, 1000);
    })
  };
  
  module.exports = { checkInventory };