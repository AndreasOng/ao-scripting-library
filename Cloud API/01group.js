var request = require('request');
var settings = require('./config.json');

console.log("Authenticating to Caseware Cloud");
login();


function login()
{
    var sUrl = settings.CloudURL + '/CWCoreService/restricted/SessionService.svc/Login';
    
    var oPayload = {
      applicationType: "Browser",
      firmName: settings.CloudFirm,
      language: "en",
      loginName: settings.UserName,
      password: settings.Password,
      rememberMe: true,
    };

   request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('Login error:', error);
            } else {
                settings.UUID = body.d.result.UUID;
                console.log("Login successful UUID: " + settings.UUID);

                console.log("Loading Security Groups");
                viewSecurityGroups();
            }
        }
    );
}

function viewSecurityGroups(){
    //https://au.casewarecloud.com/test-soeadvisory/CWCoreService/restricted/PersonService.svc/GetSecurityGroupList
     var sUrl = settings.CloudURL +'/CWCoreService/restricted/PersonService.svc/GetSecurityGroupList';
    
     var oPayload = {
        "ShowAllPeopleGroups":true,
        "F":{"SortBy":[
                {"Field":{"FieldName":"isAllPeopleGroup","Type":"BaseModel","ExpressionType":"FilterField"},"Ascending":false},
                {"Field":{"FieldName":"BriefDescription","Type":"BaseModel","ExpressionType":"FilterField"},"Ascending":true}],
                "BatchSize":31,
                "OmitTotal":true
            },
        "Uuid": settings.UUID};


     request.post({
       url : sUrl,
       body: oPayload,
       json: true
   }, function (err, response, body) {
            if(err){
                console.log('error:', error);
            } else {
                console.log('Staff group received');               
                var list = body.d.result.result;

                for(var i=0; i<list.length; i++){
                    //Search staff groups
                    if(list[i].OwnerType === 0){
                        console.log(list[i].Id + ","+list[i].BriefDescription);
                    }
                }
            }
        }
    );   
}

