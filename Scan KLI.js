clear();
scan();


function scan(){
	
	//create file to save
	var txt = new ActiveXObject("Scripting.FileSystemObject");
	var s = txt.CreateTextFile("C:\\temp\\KLI.txt", true);
	
				
	var aIndexToScan = [0,1,12,13,14,15,16];
	
	var sKLIPath = document.cell("KLIPATH").value;
	var KLI = document.kliRecordSet(sKLIPath, "Distributor");
	var TAB = '|';
	
	//print the KLI Index
	var sHeader;
	for(var f=0; f<aIndexToScan.length; f++){
		sHeader += KLI.getFieldName(aIndexToScan[f]) + TAB;
	}
	s.WriteLine(sHeader);
	//print(sHeader);
	
	var ProgressBar = document.createProgressBar("scanning", KLI.recordsCount(), 1, 0 , "scanning");
	var iCount = 0;
	var iLimit = KLI.recordsCount();
	if(KLI){
		
		//iterate through the KLI list
		var oKLIGrouping = {};
		
		//Building the array
		for(KLI.moveFirst(); !KLI.isEndOfFile(); KLI.moveNext()){
			iCount++;		
			ProgressBar.setMessage(iCount + " of" + iLimit + KLI.getFieldValue(0));
			ProgressBar.updateProgress(1);
		
			//check file name
			if(KLI.getFieldValue(1).toUpperCase().indexOf('DIST') >= 0){
				var sBody;
				for(var f=0; f<aIndexToScan.length; f++){
					sBody += KLI.getFieldValue(aIndexToScan[f]) + TAB;
				}
				s.WriteLine(sBody);
				//print(sBody);
			}
		}
	}
	
	s.Close();
}


function alert(sMsg){
	document.messagebox("Alert",sMsg, MESSAGE_OK);
}

function print(sMsg){
	var oPara = document.insertParaAfter(document.paraCount());
	oPara.insertTextAfter(0,sMsg);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount());
	document.removeSectionAndContent(oSection.index);
}