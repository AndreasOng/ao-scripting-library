clear();
//access the CaseWare Object
var CWClient = document.cwClient;
if(CWClient)
{
    var PROGRESSBAR = document.createProgressBar("Scanning",CWClient.Documents.Count, 1, 0, "Scanning");
    for(var e=1; e<=CWClient.Documents.Count; e++)
    {
        PROGRESSBAR.updateProgress(1);
        var oDoc = CWClient.Documents.Item(e);
        if(oDoc)// && oDoc.Id == "6-120")
        {
            //Check if the document is a caseview document
            if(oDoc.Type == 2) //CaseView Document
            {
                PROGRESSBAR.setMessage(oDoc.Id + " - " + oDoc.Name);
                
                //Open the document
                var sPath = CWClient.FilePath + oDoc.FileName;
                var CVDOC = Application.openDocument(sPath,4); //4 to bypass all system scripts
                if(CVDOC)
                {
                    print(oDoc.Id + " - " + oDoc.Name);
                    for (var p=1; p<=CVDOC.paraCount; p++){
                        var oPara = CVDOC.para(p);
                        
                        if(oPara){
                            var iObjectIndex = oPara.getNextObjectLocation(0,TYPEID_PICTURE);
                            while(iObjectIndex){
                                var oPicture = oPara.getObjectAt(iObjectIndex);
                                
                                if(oPicture){
                                    try{
                                        oPicture.embed = 1;
                                    }
                                    catch(err){
                                        print("Error on Para " + p + " Image: " + oPicture.fileName + " Error: "+ err.message );
                                    }

                                }
                                iObjectIndex = oPara.getNextObjectLocation(iObjectIndex + 1,TYPEID_PICTURE);
                            }	
                        }
                    }
                    
                    CVDOC.close(1);
                    print("Process completed");
                }
            }
        }
    }
}

function alert(sText){
    document.messageBox("Alert", sText, MESSAGE_OK);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}