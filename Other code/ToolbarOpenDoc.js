/**
** openDocument() will check if the document exist, and load the document
** or give an alert when the document doesn't exist
**/
function openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg)
{
	//OnClick events
	var oToolbarButton = TemplateToolbarButtons.get(sButtonID);
	if(oToolbarButton)
	{
		if(Documents.Get(sDocumentID))
		{
			//Document exist, allow the document to launch
			oToolbarButton.Url = sDocumentUrl;
		}
		else
		{
			//Document doesn't exist, alert user and clear URL
			MessageBox("Alert",sAlertMsg,MESSAGE_OK);
			oToolbarButton.Url = "";
		}
	}
}

/**
** openDocument() will check if the file exist, and load the file
** or give an alert when the file doesn't exist
**/
function openFile(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg)
{
	//OnClick events
	var oToolbarButton = TemplateToolbarButtons.get(sButtonID);
	if(oToolbarButton)
	{
		oToolbarButton.Url = sDocumentUrl;
	}
}