/**
** Template Toolbar Script for CRPT - Control Report
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the CRPT document
**/
function IsVisible()
{
	if(Documents.Get("CRPT"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "CRPT";
	var sDocumentID = "CRPT";
	var sDocumentUrl = "cw:CRPT"; 		
	var sAlertMsg = "CRPT document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}