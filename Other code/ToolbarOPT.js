/**
** Template Toolbar Script for OPT - Engagement Optimiser
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Hides the toolbar button if the Optimiser documents doesn't exist
** We will only check the document that is relevant to the profile
**
** ID		Profile            	Document ID
** 0		No Profile			
** 1		Australia			OPT
** 2		New Zealand			OPTNZ
** 3		SMSF				OPTS
** 4		Review	
**/
function IsVisible()
{
	//Change your settings Here
    var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","") 	//The profile information, should be integer

	//Enable when the profile is not defined
	if(!iProfile)
		return true;
		
	//Profile 1 and document OPT exist
	if(iProfile == 1 && Documents.Get("OPT"))
		return true;
	
	//Profile 2 and document OPTNZ exist
	if(iProfile == 2 && Documents.Get("OPTNZ"))
		return true;
	
	//Profile 3 and document OPTS exist
	if(iProfile == 3 && Documents.Get("OPTS"))
		return true;
		
	//Profile 4 and document OPTR exist
	if(iProfile == 4 && Documents.Get("OPTR"))
		return true;
		
	//Hide the button
	return false;
}

/**
** The event need to be OnClick() event
** Checks the profile, and set the URL to the right
** document ID
**
** This will work because the Template Toolbar Button actions are executed in the following 
** order: Script, Script File, Url. So the above script will execute and set the URL, after 
** which the document will be launched.
**
** ID		Profile            	Document ID
** 0		No Profile			
** 1		Australia			cw:OPT
** 2		New Zealand			cw:OPTNZ
** 3		SMSF				cw:OPTS
** 4		Review			
**/
function OnClick() 
{
    //Change your settings Here
    var sButtonID = "OPT";												//The button ID
    var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","") 	//The profile information, should be integer
    var aDocID = new Array("","OPT","OPTNZ","OPTS","OPTR");					//The document list
	var aDocUrl = new Array("","cw:OPT","cw:OPTNZ","cw:OPTS","cw:OPTR");		//The document list

	/*** START OF THE SELECTION PROCESS ***/
	//Check that the Profile exist in the array
	if(iProfile > 0 && iProfile < aDocID.length)
	{
		//Set the default URL 
		openDocument(sButtonID, aDocID[iProfile], aDocUrl[iProfile], "Document " + aDocID[iProfile] + " is missing." );
	}
}
