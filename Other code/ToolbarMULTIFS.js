/**
** Template Toolbar Script for MULTIFS - Multiple Financial Statements
**/

/**
** Disable the button whent there is no financial statements in the document
**/
function IsVisible()
{
	if(Documents.Get("FSIFRS"))
		return true;
	else
		return false;
}