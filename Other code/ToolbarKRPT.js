/**
** Template Toolbar Script for KRPT - Key Audit Matters Documentation
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the KRPT document
**/
function IsVisible()
{
	if(Documents.Get("KRPT"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "KRPT";
	var sDocumentID = "KRPT";
	var sDocumentUrl = "cw:KRPT"; 		
	var sAlertMsg = "KRPT document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}