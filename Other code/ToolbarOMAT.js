/**
** Template Toolbar Script for OMAT - Materiality
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the 2-200 document
**/
function IsVisible()
{
	if(Documents.Get("2-200"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "OMAT";
	var sDocumentID = "2-200";
	var sDocumentUrl = "cw:2-200";		//"cw:cvMAT00000AUAU";
	var sAlertMsg = "Materiality document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}