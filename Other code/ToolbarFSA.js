/**
** Template Toolbar Script for FSA - Financial Statements Audit
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the FSA document
**/
function IsVisible()
{
	if(Documents.Get("FSA"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "FSA";
	var sDocumentID = "FSA";
	var sDocumentUrl = "cw:FSA"; 		//"cw:cv00000100CWCW";
	var sAlertMsg = "FSA document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}