/**
** Template Toolbar Script for OCR - Confirmation Report
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the FSA document
**/
function IsVisible()
{
	if(Documents.Get("OCR"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "OCR";
	var sDocumentID = "OCR";
	var sDocumentUrl = "cw:OCR"; 		//"cw:cv0000AOCRCWCW";
	var sAlertMsg = "OCR document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}