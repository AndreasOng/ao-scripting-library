/**
** Template Toolbar Script for MRPT - Reportable Items Report
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the MRPT document
**/
function IsVisible()
{
	if(Documents.Get("MRPT"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "MRPT";
	var sDocumentID = "MRPT";
	var sDocumentUrl = "cw:MRPT"; 		//"cw:cvRPTITEMSCWCW";
	var sAlertMsg = "MRPT document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}