/**
** Template Toolbar Script for RRPT - Risk Report
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the RRPT document
**/
function IsVisible()
{
	if(Documents.Get("RRPT"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "RRPT";
	var sDocumentID = "RRPT";
	var sDocumentUrl = "cw:RRPT";	//"cw:cvRRPT0000CWCW";
	var sAlertMsg = "RRPT document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}