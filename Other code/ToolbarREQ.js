/**
** Template Toolbar Script for OPT - Engagement Optimiser
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Hides the toolbar button if the Optimiser documents doesn't exist
** We will only check the document that is relevant to the profile
**
** The code is sequential to minimise memory use
**
** ID		Profile            	Document ID
** 0		No Profile			
** 1		Australia			6-100
** 2		New Zealand			6-100NZ
** 3		SMSF				C-250
** 4		Review	
**/
function IsVisible()
{
	//Change your settings Here
    var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","") 	//The profile information, should be integer
	
	//Enable when the profile is not defined
	if(!iProfile)
		return true;
		
	//Profile 1 and document 6-100 exist
	if(iProfile == 1 && Documents.Get("6-100"))
		return true;
	
	//Profile 2 and document 6-100NZ exist
	if(iProfile == 2 && Documents.Get("6-100NZ"))
		return true;
	
	//Profile 3 and document C-250 exist
	if(iProfile == 3 && Documents.Get("C-250"))
		return true;
		
	//Hide the button
	return false;
}

/**
** The event need to be OnClick() event
** Checks the profile, and set the URL to the right
** document ID
**
** This will work because the Template Toolbar Button actions are executed in the following 
** order: Script, Script File, Url. So the above script will execute and set the URL, after 
** which the document will be launched.
**
** ID		Profile            	Document ID
** 0		No Profile			
** 1		Australia			cw:6-100
** 2		New Zealand			cw:6-100NZ
** 3		SMSF				cw:C-250
** 4		Review			
**/
function OnClick() 
{
    //Change your settings Here
    var sButtonID = "ASA";												//The button ID
    var iProfile = CaseViewData.DataGroupFormId("PROFILE","INDEX","") 	//The profile information, should be integer
    var aDocID  = new Array("","6-100","6-100NZ","C-250","");	
	var aDocUrl = new Array("","cw:6-100","cw:6-100NZ","cw:C-250","");		//The document list

	/*** START OF THE SELECTION PROCESS ***/
	//Check that the Profile exist in the array
	if(iProfile > 0 && iProfile < aDocID.length-1)
	{
		//Set the default URL 
		openDocument(sButtonID, aDocID[iProfile], aDocUrl[iProfile], "Document " + aDocID[iProfile] + " is missing." );
	}
}
