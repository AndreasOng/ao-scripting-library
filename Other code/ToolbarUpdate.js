/**
** Template Toolbar Script for UPDATE - Update Document
**/

/**
** Include additional external scripting
**/
include("ToolbarOpenDoc.js");

/**
** Disable the button whent he user deleted the UPDATE document
**/
function IsEnabled()
{	

	if(Documents.Get("UPDATE"))
		return true;
	else
		return false;
}

/**
** OnClick button will check whether the document exist before launching the document
** If the document doesn't exist give out an alert to the user
**/
function OnClick()
{
	//Settings Area
	var sButtonID = "UPDATE";
	var sDocumentID = "UPDATE";
	var sDocumentUrl = "cw:UPDATE";		//"cw:cv000WPGUDCWCW";
	var sAlertMsg = "Update document is missing";
	
	//Call the open document function
	openDocument(sButtonID, sDocumentID, sDocumentUrl, sAlertMsg);
}