
function doCWANZMetrics(){

//document.messagebox("Alert", "START",1);	
	var WP = document.cwClient;
	
	var oDocList = {};
		
		//go through the folder and compare the ID
		for(var e = new Enumerator(WP.documents); !e.atEnd(); e.moveNext()){
			var oDoc = e.item();
			
			//is it a folder
			if(oDoc.type != 0) { //dtFolder
				//Lets collect the information
				
				var oDocDetails = {
					"id" 				: 	oDoc.Id,
					"name"				:	oDoc.name,
					"type"				:	oDoc.Type,
					"signoffdate1"		:	new Date(oDoc.SignoffDate(1)),
					"signoffInitials1"	:	oDoc.SignoffInitials(1),
					"signoffdate2"		:	new Date(oDoc.SignoffDate(2)),
					"signoffInitials2"	:	oDoc.SignoffInitials(2),
					"signoffdate3"		:	new Date(oDoc.SignoffDate(3)),
					"signoffInitials3"	:	oDoc.SignoffInitials(3),
					"signoffdate4"		:	new Date(oDoc.SignoffDate(4)),
					"signoffInitials4"	:	oDoc.SignoffInitials(4),
					"signoffdate5"		:	new Date(oDoc.SignoffDate(5)),
					"signoffInitials5"	:	oDoc.SignoffInitials(5),
					"signoffdate6"		:	new Date(oDoc.SignoffDate(6)),
					"signoffInitials6"	:	oDoc.SignoffInitials(6),
					"signoffdate7"		:	new Date(oDoc.SignoffDate(7)),
					"signoffInitials7"	:	oDoc.SignoffInitials(7),
					"signoffdate8"		:	new Date(oDoc.SignoffDate(8)),
					"signoffInitials8"	:	oDoc.SignoffInitials(8),
					"isdeliverable"		: 	oDoc.Deliverable,
					"delivereddate"		: 	new Date(oDoc.DeliveredDate),
					"duedate"			:	new Date(oDoc.DueDate)
					};
				
				oDocList[oDoc.Id] = oDocDetails;
			}//IF
			
		}
		
		//document.messagebox("Alert", "END",1);
		//store the information
		WP.CaseViewData.DataGroupFormId("METADATA","CWANZ","DOCUMENTS") = JSON.stringify(oDocList);
		
		//document.messagebox("Alert", "END2",1);	

}