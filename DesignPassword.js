
//Iterate through document manager
var sPassword = document.cell("PASS").value; //"99B0ttles";

//access the CaseWare Object
var CWClient = document.cwClient;
if(CWClient)
{
    var PROGRESSBAR = document.createProgressBar("Scanning",CWClient.Documents.Count, 1, 0, "Scanning");
    for(var e=1; e<=CWClient.Documents.Count; e++)
    {
        PROGRESSBAR.updateProgress(1);
        var oDoc = CWClient.Documents.Item(e);
        if(oDoc)// && oDoc.Id == "6-120")
        {
            //Check if the document is a caseview document
            if(oDoc.Type == 2) //CaseView Document
            {
                PROGRESSBAR.setMessage(oDoc.Id + " - " + oDoc.Name);
                
                //Open the document
                var sPath = CWClient.FilePath + oDoc.FileName;
                var CVDOC = Application.openDocument(sPath,4); //4 to bypass all system scripts
                if(CVDOC)
                {
                    //check password
                    try{
                        CVDOC.unlockDesign(sPassword);
                        print(oDoc.Id + "OK");
                    }
                    catch(err){
                        //print(oDoc.Id + " - " + oDoc.Name + "FAILED");
                    }
                    CVDOC.close(0);
                }
            }
        }
    }
}

function alert(sText){
    document.messageBox("Alert", sText, MESSAGE_OK);
}

function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount);
	document.removeSectionAndContent(oSection.index());
}


function print(sText){
	var oPara = document.insertParaAfter(document.paraCount);
	oPara.insertTextAfter(0,sText);
}