/**
 * This script is designed to address the script error on the Crowe Horwath template.
 * The error is caused by our script called Util. This is now used by CaseWare International.
 * The goal of this script is to change the script name and update the events in the document
 *
 * Version 1.0		18 11 2020
 */
 
 var bLog = document.cell("LOG").value;
 var oProgressBar = document.createProgressBar("Updating",1,1,0,"Updating");
 clear();
 process();
 
 
 function process(){
	
	//Step 1. Go through the document managers
	var oCW = document.cwClient;
	
	for(var oDocs = new Enumerator(oCW.documents); !oDocs.atEnd(); oDocs.moveNext()){
		
		//get a document
		var oDoc = oDocs.item();
		
		if(containsTag(oDoc, ["Crowe", "TT Flat-form"])){
			
			//Filter the materiality document
			if(oDoc.Name.toUpperCase().indexOf("MATERIALITY") < 0){
				print(oDoc.Name);
				oProgressBar.setTitle(oDoc.Name);
				oProgressBar.setMessage(oDoc.Name);
				updateDocument(oCW.FilePath + "\\" + oDoc.FileName);
			}			
		}
		
	}
 }
 
 function updateDocument(sPath){
	oProgressBar.setMessage("Opening");
	var oDoc = Application.openDocument(sPath);
	print("- Opening");
	
	//update script
	var iCount = 0;
	oProgressBar.setMessage("Updating Script");
	for(var i=1; i<oDoc.scriptCount(); i++){
		
		//find the right script
		try{
			var oScript = oDoc.scriptByIndex(i,"Blu3b1rd");
			if(oScript.name == "Util" && oScript.fLibrary == 0 && oScript.repository == 0){
				
				oScript.name = "CWANZUtil";
				iCount++;
			}
		}
		catch(err){
			//Ignore
		}
	}
	print("- " + iCount + " Script processed");
	
	//Update buttons
	oProgressBar.setMessage("Updating Buttons");
	iCount = 0;
	var oOldCell = document.cell("OLDB");
	var oNewCell = document.cell("NEWB");

	for(var c=1; c<oDoc.cellCount(); c++){
		var oCell = oDoc.cellByIndex(c);
		
		if(oCell.type == TYPE_BUTTON && oCell.calculation == "\"+\"" && oCell.eventOnClick.indexOf("Util") >= 0 ){
			
			//update the event
			oCell.eventOnClick = oNewCell.eventOnClick;
			iCount++;
		}
	}
	print("- " + iCount + " Buttons processed");
	
	//Update buttons
	oProgressBar.setMessage("Save and close");
	oDoc.close(1);
	print("- Closed");
 }
 
 function containsTag(oDocument, aTags){
	
	//No tags, ignore it
	if(oDocument.Type != 2){
		//Not CaseView Document
		return false;
	}
	else{
		//get the tags object
		var oTags = oDocument.Tags
	
		//check the tags
		for(var i=0; i<aTags.length; i++){
			if(!oTags.Get(aTags[i])){
				
				//tag not found return falser
				return false;
			}
		}
	}
	
	//we are here, all good
	return true;
 }
 
 function print(sMessage){
	
	//get the last para
	oPara = document.insertParaAfter(document.paraCount());
	oPara.insertTextAfter(0, sMessage);
 }
 
 function clear(){
	var oSection = document.insertSectionAt(1,document.paraCount());
	document.removeSectionAndContent(oSection.index());
 }